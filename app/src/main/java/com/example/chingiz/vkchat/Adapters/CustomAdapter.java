package com.example.chingiz.vkchat.Adapters;

/**
 * Created by Chingiz on 11.02.2018.
 */

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.chingiz.vkchat.ExtendedAPI.VKDocCheckable;
import com.example.chingiz.vkchat.R;
import com.vk.sdk.api.model.VKList;

public class CustomAdapter extends ArrayAdapter {

    private VKList<VKDocCheckable> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        CheckBox checkBox;
    }

    public CustomAdapter(VKList data, Context context) {
        super(context, R.layout.documents_row, data);
        this.dataSet = data;
        this.mContext = context;

    }
    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public VKDocCheckable getItem(int position) {
        return dataSet.get(position);
    }


    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {


        ViewHolder viewHolder;
        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.documents_row, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);

            result = convertView;
            convertView.setTag(viewHolder);


        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        VKDocCheckable item = getItem(position);


        viewHolder.txtName.setText(item.getType());
        viewHolder.checkBox.setChecked(item.getChecked());



        return result;
    }
}