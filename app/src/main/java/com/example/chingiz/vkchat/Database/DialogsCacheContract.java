package com.example.chingiz.vkchat.Database;

import android.provider.BaseColumns;

/**
 * Created by chingiz on 11.04.2016.
 */
class DialogsCacheContract {

    /* Inner class that defines the table contents */
    public static abstract class DialogsCacheEntry implements BaseColumns {


        public static final String TABLE_NAME = "dialogs_cache";
        public static final String _ID = "_id";

        public static final String ID = "id";
        public static final String USER_ID = "user_id"; //идентификатор пользователя, в диалоге с которым находится сообщение.
        public static final String FROM_ID = "from_id"; //идентификатор автора сообщения.
        public static final String BODY = "body"; //текст сообщения.
        public static final String OUT = "out"; //тип сообщения (0 — полученное, 1 — отправленное, не возвращается для пересланных сообщений).
        public static final String READ_STATE = "read_state"; //статус сообщения (0 — не прочитано, 1 — прочитано, не возвращается для пересланных сообщений).
        public static final String TITLE = "title"; //заголовок сообщения или беседы.
        public static final String DATE = "date"; //дата отправки сообщения в формате unixtime.
        public static final String CHAT_ID = "chat_id"; // идентификатор беседы (если есть)
        public static final String CHAT_PHOTO_100 = "photo_100"; // фото беседы (если есть)
        public static final String UNREAD = "unread"; //кол-во непрочитанных сообщений




    }
}
