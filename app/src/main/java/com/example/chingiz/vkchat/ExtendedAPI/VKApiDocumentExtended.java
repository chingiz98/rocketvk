package com.example.chingiz.vkchat.ExtendedAPI;

import com.vk.sdk.api.model.VKApiDocument;

import org.json.JSONException;
import org.json.JSONObject;

public class VKApiDocumentExtended extends VKApiDocument {

    public VKApiDocumentPreview preview;

    public VKApiDocumentExtended() {

    }


    public VKApiDocumentExtended(JSONObject from) throws JSONException
    {
        parse(from);
    }


    @Override
    public VKApiDocumentExtended parse(JSONObject jo) {
        super.parse(jo);
        try {
            preview = new VKApiDocumentPreview(jo.optJSONObject("preview"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this;
    }
}
