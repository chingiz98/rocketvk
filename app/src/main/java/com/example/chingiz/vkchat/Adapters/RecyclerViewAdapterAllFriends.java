package com.example.chingiz.vkchat.Adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.chingiz.vkchat.Activities.DialogActivity;
import com.example.chingiz.vkchat.Interfaces.ItemClickListener;
import com.example.chingiz.vkchat.R;
import com.example.chingiz.vkchat.Util.UnixTimeParser;
import com.squareup.picasso.Picasso;
import com.turingtechnologies.materialscrollbar.INameableAdapter;
import com.vk.sdk.api.model.VKUsersArray;


import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by chingiz on 08.09.2015.
 */

public class RecyclerViewAdapterAllFriends extends RecyclerView.Adapter<RecyclerViewAdapterAllFriends.ViewHolder> implements INameableAdapter {

    private final Context mContext;
    private final VKUsersArray friends;
    //private final VKUsersArray online_friends;


    public RecyclerViewAdapterAllFriends(Context context, VKUsersArray friends) {
        this.mContext = context;
        this.friends = friends;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.online_friend_item, viewGroup, false);


        ViewHolder holder = new ViewHolder(v);

        holder.setIsRecyclable(false);
        return holder;
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);


    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {


        viewHolder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if(!isLongClick){
                    //Toast.makeText(mContext, "#" + position + " ", Toast.LENGTH_SHORT).show();


                    mContext.startActivity(new Intent(mContext, DialogActivity.class).putExtra("user",friends.get(viewHolder.getAdapterPosition())));


                } else {
                    //Toast.makeText(mContext, "#" + position + " - (Long click)", Toast.LENGTH_SHORT).show();
                }
            }
        });

        viewHolder.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, DialogActivity.class).putExtra("user",friends.get(viewHolder.getAdapterPosition())));

            }
        });

        //friends_cursor.moveToPosition(i);


        String name = friends.get(viewHolder.getAdapterPosition()).first_name;
        String last = friends.get(viewHolder.getAdapterPosition()).last_name;

        //viewHolder.pic.setVisibility(View.INVISIBLE);
        if(friends.get(i).online){
            viewHolder.last_seen.setText(mContext.getResources().getString(R.string.online));
        } else {
            //viewHolder.online.setImageDrawable(null);
            viewHolder.pic.setBorderWidth(0);
            viewHolder.last_seen.setVisibility(View.VISIBLE);
            viewHolder.last_seen.setText((new UnixTimeParser(mContext, friends.get(i).last_seen)).getDateString());

        }




           try {
               Picasso.with(mContext).load(friends.get(viewHolder.getAdapterPosition()).photo_100).into(viewHolder.pic);
           } catch (Exception e){
                System.out.println(e.getMessage());
           }

        //img = new ImageDownloaderTask(viewHolder.pic);
        //img.execute(usr_arr.get(i).photo_100);

        //viewHolder.pic.setImageBitmap(bmp);

        String full_name = name + " " + last;
        viewHolder.name.setText(full_name);
        //Log.d("MSG!!!!!!!!!!!", usr_arr.get(i).first_name);
    }




    @Override
    public int getItemCount() {
       if (friends != null) {
            return friends.getCount();
       }
        return 0;
    }


    @Override
    public Character getCharacterForElement(int element) {
        return friends.get(element).first_name.charAt(0);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private final TextView name;
        //ImageView  online;
        final CircleImageView pic;
        final TextView last_seen;
        ImageView info;


        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.online_friend_name);
            pic = (CircleImageView) itemView.findViewById(R.id.online_friend_pic);
            //online = (ImageView) itemView.findViewById(R.id.imageOnline);
            last_seen = (TextView) itemView.findViewById(R.id.last_seen);
            info = (ImageView) itemView.findViewById(R.id.infoProfile);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }


        private ItemClickListener clickListener;

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }


        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onClick(v, getAdapterPosition(), true);
            return true;
        }
    }

}


