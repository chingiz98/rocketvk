package com.example.chingiz.vkchat.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chingiz.vkchat.Activities.DialogActivity;
import com.example.chingiz.vkchat.Interfaces.ItemClickListener;
import com.example.chingiz.vkchat.R;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKUsersArray;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by chingiz on 07.07.2016.
 */
public class RecyclerViewAdapterOnlineFriends extends RecyclerView.Adapter<RecyclerViewAdapterOnlineFriends.ViewHolder> {

    private final Context mContext;
    private final VKUsersArray online_friends;
    private final VKUsersArray all_friends;


    public RecyclerViewAdapterOnlineFriends(Context context, VKUsersArray all_friends, VKUsersArray online_friends) {
        mContext = context;
        this.online_friends = online_friends;
        this.all_friends = all_friends;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.online_friend_item, viewGroup, false);
        Log.d("MSG!!!!!!!!!!!", "FRIENDS VIEWHOLDER");

        ViewHolder holder = new ViewHolder(v);

        holder.setIsRecyclable(false);
        return holder;
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);

        System.out.println("RECYCLED");
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
    viewHolder.last_seen.setText(mContext.getString(R.string.online));

        viewHolder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if(!isLongClick){
                    //Toast.makeText(mContext, "#" + position + " ", Toast.LENGTH_SHORT).show();


                    mContext.startActivity(new Intent(mContext, DialogActivity.class).putExtra("user", all_friends.getById(online_friends.get(i).id)));


                } else {
                    //Toast.makeText(mContext, "#" + position + " - (Long click)", Toast.LENGTH_SHORT).show();
                }
            }
        });

        viewHolder.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, DialogActivity.class).putExtra("user", all_friends.getById(online_friends.get(i).id)));
            }
        });

        //friends_cursor.moveToPosition(i);

        String name = "default name";
        String last = "default last name";

        try {
            name = all_friends.getById(online_friends.get(viewHolder.getAdapterPosition()).id).first_name;
            last = all_friends.getById(online_friends.get(viewHolder.getAdapterPosition()).id).last_name;
            //viewHolder.pic.setVisibility(View.INVISIBLE);
        } catch (Exception e){

        }



        try {
            Picasso.with(mContext).load(all_friends.getById(online_friends.get(viewHolder.getAdapterPosition()).id).photo_100).into(viewHolder.pic);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

        //img = new ImageDownloaderTask(viewHolder.pic);
        //img.execute(usr_arr.get(i).photo_100);

        //viewHolder.pic.setImageBitmap(bmp);

        String full_name = name + " " + last;
        viewHolder.name.setText(full_name);
        //Log.d("MSG!!!!!!!!!!!", usr_arr.get(i).first_name);
    }




    @Override
    public int getItemCount() {
        if (online_friends != null) {
            return online_friends.getCount();
        }
        return 0;
    }



    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private final TextView name;
        //ImageView  online;
        final CircleImageView pic;
        final TextView last_seen;

        private ImageView info;


        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.online_friend_name);
            pic = (CircleImageView) itemView.findViewById(R.id.online_friend_pic);
            //online = (ImageView) itemView.findViewById(R.id.imageOnline);

            info = (ImageView) itemView.findViewById(R.id.infoProfile);
            last_seen = (TextView) itemView.findViewById(R.id.last_seen);



            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }


        private ItemClickListener clickListener;

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }


        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onClick(v, getAdapterPosition(), true);
            return true;
        }
    }

}



