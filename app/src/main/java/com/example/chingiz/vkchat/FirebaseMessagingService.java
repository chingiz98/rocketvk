package com.example.chingiz.vkchat;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.chingiz.vkchat.Activities.MainActivity;
import com.example.chingiz.vkchat.Database.CacheDB;
import com.google.firebase.messaging.RemoteMessage;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.methods.VKApiCaptcha;
import com.vk.sdk.api.methods.VKApiUsers;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;
import com.vk.sdk.api.model.VKUsersArray;

import java.util.Map;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService{

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d("MSG", "FCM RECEIVED: " + remoteMessage.toString());
        processNotification(remoteMessage.getData());
    }

    private void processNotification(Map <String, String> data) {

        final Intent i = new Intent(this,MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int uid = Integer.parseInt(data.get("uid"));
        final int msg_id = Integer.parseInt(data.get("msg_id"));
        final int badge = Integer.parseInt(data.get("badge"));
        final String text = data.get("text");
        String title = "TITLE";
        boolean isUserFound = false;

        VKUsersArray friends = ((MyApp) getApplicationContext()).getFriends();
        VKUsersArray users = ((MyApp) getApplicationContext()).getNewUsers();

        if(friends.getById(uid) != null){
            title = friends.getById(uid).first_name + " " + friends.getById(uid).last_name;
            isUserFound = true;
        } else if(users.getById(uid) != null){
            title = users.getById(uid).first_name + " " + users.getById(uid).last_name;
            isUserFound = true;
        } else {

            VKRequest get_user = VKApi.users().get(VKParameters.from("user_ids", uid, VKApiConst.FIELDS, MainActivity.USER_FIELDS));
            get_user.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    super.onComplete(response);
                    VKList<VKApiUserFull> resp = (VKList<VKApiUserFull>) response.parsedModel;
                    showNotification(i, text, resp.get(0).first_name + " " + resp.get(0).last_name, msg_id, badge);
                    //CacheDB db = new CacheDB(getApplicationContext());
                    //db.addUsers(resp, false);

                }
            });
        }

        if(isUserFound)
            showNotification(i, text, title, msg_id, badge);


    }

    private void showNotification(Intent i, String text, String title, int msg_id, int badge){

        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(text)
                .setSmallIcon(R.mipmap.rocket)
                .setContentIntent(pendingIntent);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(0,builder.build());
    }




}