package com.example.chingiz.vkchat.LongPoll;

import com.vk.sdk.api.VKRequest;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 22.09.2015.
 */
public class LongPollServer {
    public String key;
    public String ts;
    public String server;


    public void initializeLongPollServer(VKRequest.VKRequestListener listener){
        String REQUEST = "messages.getLongPollServer";
        VKRequest req = new VKRequest(REQUEST);
        req.executeWithListener(listener);


    }

    public void parseLongPoll(String response) throws JSONException {
        JSONObject json = new JSONObject(response);
        JSONObject jsonMessage = json.getJSONObject("response");

        this.key = jsonMessage.getString("key");
        this.server = jsonMessage.getString("server");
        this.ts = String.valueOf(jsonMessage.getLong("ts"));

        System.out.println("LONG POLL SERVER IS " + this.server);





    }


}
