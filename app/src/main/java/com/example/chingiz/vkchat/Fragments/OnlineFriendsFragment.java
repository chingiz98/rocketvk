package com.example.chingiz.vkchat.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.chingiz.vkchat.Adapters.RecyclerViewAdapterOnlineFriends;
//import com.example.chingiz.vkchat.Database.FriendsCacheDbHelper;
import com.example.chingiz.vkchat.MyApp;
import com.example.chingiz.vkchat.R;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKUsersArray;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Admin on 12.09.2015.
 */
public class OnlineFriendsFragment extends Fragment  {


    private RecyclerViewAdapterOnlineFriends adapter;
    //public VKUsersArray friends;
    //public JSONArray online_friends;
    public boolean online = true;


    public void notifyDataSetChanged(){
        adapter.notifyDataSetChanged();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        VKUsersArray friends = ((MyApp) getContext().getApplicationContext()).getFriends();
        VKUsersArray online_friends = ((MyApp) getContext().getApplicationContext()).getOnlineFriends();
        View v = inflater.inflate(R.layout.layout2, container, false);
        RecyclerView recycler = (RecyclerView) v.findViewById(R.id.recycler_friends_online);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new RecyclerViewAdapterOnlineFriends(getActivity(), friends, online_friends);
        recycler.setAdapter(adapter);
      /*  if(online){


        }
*/
        return v;
    }

    private VKUsersArray getOnlineFriendsArray(JSONArray online_friends) throws JSONException {

        VKUsersArray online_friends_arr = new VKUsersArray();

        for(int i = 0; i < online_friends.length(); i++){
            VKApiUserFull usr = new VKApiUserFull();
            usr.id = online_friends.getInt(i);
            online_friends_arr.add(usr);
        }

        return online_friends_arr;
    }



}
