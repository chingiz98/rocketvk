package com.example.chingiz.vkchat.Adapters.AttachmentsCards;

import android.util.Log;

import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiDocument;
import com.vk.sdk.api.model.VkAudioArray;
import com.vk.sdk.api.photo.VKUploadMessagesPhotoRequest;
import com.vk.sdk.util.VKUtil;

import org.json.JSONObject;

import java.io.File;

/**
 * Created by Chingiz on 17.08.17.
 */

public class AttachmentCardPhoto extends AttachmentCard {
    private final String PHOTO = "photo";
    private String src;


    public AttachmentCardPhoto(String src){
        this.src = src;
    }

    public String getPath(){
        return src;
    }

    @Override
    public String getURI() {
        return PHOTO + owner_id + "_" + item_id;
    }

    @Override
    public void startUpload(VKRequest.VKRequestListener listener) {
        File uploadImg = new File(src);
        uploadRequest = new VKUploadMessagesPhotoRequest(uploadImg);
        uploadRequest.executeWithListener(listener);


    }
}
