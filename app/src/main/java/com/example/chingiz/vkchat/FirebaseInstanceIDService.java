package com.example.chingiz.vkchat;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;


public class FirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onCreate() {
        //Register token if it is first run
        String token = FirebaseInstanceId.getInstance().getToken();
        if(!getRegistrationStateFromPrefs() && token!=null) registerToken(token);

    }

    @Override
    public void onTokenRefresh() {

        new VKCallback<VKSdk.LoginState>(){

            @Override
            public void onResult(VKSdk.LoginState res) {
                //If user is already logged in and need to register token again
                if(res == VKSdk.LoginState.LoggedIn){
                    String token = FirebaseInstanceId.getInstance().getToken();
                    registerToken(token);
                }
            }

            @Override
            public void onError(VKError error) {

            }
        };


    }


    private void registerToken(final String token) {

        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Log.d("ANDROID_ID", android_id);

        VKRequest request = new VKRequest("account.registerDevice", VKParameters.from("token", token, "settings", " {\"msg\":\"on\"}", "device_id", android_id));
        request.executeWithListener(new VKRequest.VKRequestListener() {

            @Override
            public void onError(VKError error) {
                Log.d("MSG", "SHIT ERROR:" + error.toString());
                super.onError(error);
            }

            @Override
            public void onComplete(VKResponse response) {
                saveRegistrationState(true);
                Log.d("MSG", "SHIT NIGGER " + response.json.toString());
                VKRequest request2 = new VKRequest("account.getPushSettings", VKParameters.from("token", token));
                request2.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        System.out.println("REQUEST COMPLETED");
                        System.out.println("RESPONSE " + response.responseString);
                    }



                    @Override
                    public void onError(VKError error) {
                        System.out.println("ERROR APPEARED");
                        System.out.println("RESPONSE " + error.errorMessage);
                    }
                });
            }
        });


        Log.d("FCM", token);
    }

    private void saveRegistrationState(boolean state)
    {
        // Access Shared Preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();

        // Save to SharedPreferences
        editor.putBoolean("registration_state", state);
        editor.apply();
    }

    private boolean getRegistrationStateFromPrefs()
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        return preferences.getBoolean("registration_state", false);
    }


}