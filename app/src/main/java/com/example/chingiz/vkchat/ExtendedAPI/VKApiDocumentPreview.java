package com.example.chingiz.vkchat.ExtendedAPI;

import com.vk.sdk.api.model.VKApiDocument;
import com.vk.sdk.api.model.VKApiPhoto;

import org.json.JSONException;
import org.json.JSONObject;

public class VKApiDocumentPreview {


    public VKApiPhoto photo;
    //public VKApiGraffiti;
    public VKApiAudioMsg audio;

    public VKApiDocumentPreview (JSONObject from) throws JSONException {
        parse(from);
    }

    public VKApiDocumentPreview parse(JSONObject jo) throws JSONException{
        if(jo.optJSONObject("photo") != null)
            photo = new VKApiPhoto(jo.optJSONObject("photo"));

        if(jo.optJSONObject("audio_msg") != null)
            audio = new VKApiAudioMsg(jo.optJSONObject("audio_msg"));
        return this;
    }

}
