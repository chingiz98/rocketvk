package com.example.chingiz.vkchat.Database;

import android.provider.BaseColumns;

/**
 * Created by chingiz on 09.10.2015.
 */

class UsersCacheContract {

    /* Inner class that defines the table contents */
    public static abstract class FriendsCacheEntry implements BaseColumns {


        public static final String TABLE_NAME = "users_cache";
        public static final String _ID = "_id";

        public static final String ID = "id";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String BDATE = "bdate";
        public static final String ONLINE = "online";
        public static final String PHOTO_100 = "photo_100";
        public static final String PHOTO_MAX_ORIG = "photo_max_orig";
        public static final String ORDER = "_order";
        public static final String HINTS = "hints";
        public static final String FRIEND = "friend";

        public static final String LAST_SEEN = "last_seen";
        public static final String STATUS = "status";
        public static final String SEX = "sex";
        public static final String CITY = "city";
        public static final String COUNTRY = "country";
        public static final String RELATION = "relation";
        public static final String CONTACTS = "contacts";
        public static final String MOBILE_PHONE = "mobile_phone";
        public static final String HOME_PHONE = "home_phone";
        public static final String UNIVERSITIES = "universities";



    }
}
