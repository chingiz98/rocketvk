package com.example.chingiz.vkchat.Adapters;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.chingiz.vkchat.ExtendedAPI.VKApiAudioMsg;
import com.example.chingiz.vkchat.ExtendedAPI.VKApiDocumentExtended;
import com.example.chingiz.vkchat.R;
import com.example.chingiz.vkchat.Util.UnixTimeParser;
import com.google.android.flexbox.FlexboxLayout;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiDialog;
import com.vk.sdk.api.model.VKApiDocument;
import com.vk.sdk.api.model.VKApiMessage;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKAttachments;
import com.vk.sdk.api.model.VKList;

import java.io.IOException;

/**
 * Created by chingiz on 22.04.2016.
 */
public class RVMessagesAdapter extends RecyclerView.Adapter<RVMessagesAdapter.ViewHolder> {


    private final VKList<VKApiMessage> messages;
    private final VKList<VKApiMessage> messages_queue;
    private final Context mContext;

    private final int OUT = 1;
    private final int INCOMING = 0;

    private final int TYPE_FOOTER = 2;

    public  RVMessagesAdapter(Context mContext, VKList<VKApiMessage> messages, VKList<VKApiMessage> messages_queue){
        this.mContext = mContext;
        this.messages = messages;
        this.messages_queue = messages_queue;
    }


    @Override
    public RVMessagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        ViewHolder holder = null;

        if(viewType == 0){
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_msg_in, parent, false);
            holder = new InViewHolder(v);
        }

        if(viewType == 1){
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_msg_out, parent, false);
            holder = new OutViewHolder(v);
        }

        if(viewType == TYPE_FOOTER){
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.msg_progress_footer, parent, false);
            holder = new FooterViewHolder(v);
        }

        return holder;
    }



    int processPhotoAttachments1(int position, ViewHolder holder){
        int j = 0;
            Log.d("ATTACHMENT", messages.get(position).attachments.get(0).getType());

            LinearLayout first = new LinearLayout(mContext);
            //LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            first.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            first.setOrientation(LinearLayout.VERTICAL);


            ImageView firstImage = new ImageView(mContext);

            LinearLayout.LayoutParams firstImageParams = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            firstImageParams.setMargins(2,2,2,2);

            firstImage.setLayoutParams(firstImageParams);
            firstImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            firstImage.setMinimumHeight(300);
            firstImage.setMinimumWidth(604);

            VKApiPhoto photo = (VKApiPhoto) messages.get(position).attachments.get(0);
            Picasso.with(mContext).load(photo.photo_604).into(firstImage);
            first.addView(firstImage);
            //holder_in.imagesLayout.addView(first);

            if(holder.getItemViewType() == INCOMING){
                ((InViewHolder) holder).imagesLayout.addView(first);
            } else if(holder.getItemViewType() == OUT){
                ((OutViewHolder) holder).imagesLayoutOut.addView(first);
            }




            LinearLayout second = new LinearLayout(mContext);
            LinearLayout third = new LinearLayout(mContext);
            second.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150));
            second.setOrientation(LinearLayout.HORIZONTAL);
            third.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150));
            third.setOrientation(LinearLayout.HORIZONTAL);

            int size = 0;

            for(int i = 0; i < messages.get(position).attachments.size(); i++){
                if(messages.get(position).attachments.get(i).getType().equals(VKAttachments.TYPE_PHOTO)){
                    size++;
                }
            }

            //int size = messages.get(position).attachments.size();

            if(size > 1){
                int count2 = (size - 1) / 2;
                int count3 = (size - 1) - count2;

                second.setWeightSum(count2);
                third.setWeightSum(count3);


                //0  1 2 3 4   5 6 7 8 9


                j = 1;
                for(int i = 0; i < count2; i++){
                    ImageView img = new ImageView(mContext);
                    VKApiPhoto ph = (VKApiPhoto) messages.get(position).attachments.get(j);
                    Picasso.with(mContext).load(ph.photo_604).into(img);

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    params.weight = 1.0f;
                    params.setMargins(2,2,2,2);
                    img.setLayoutParams(params);
                    img.setScaleType(ImageView.ScaleType.CENTER_CROP);

                    second.addView(img);
                    j++;
                }
                for(int i = 0; i < count3; i++){
                    ImageView img = new ImageView(mContext);
                    VKApiPhoto ph = (VKApiPhoto) messages.get(position).attachments.get(j);
                    Picasso.with(mContext).load(ph.photo_604).into(img);

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    params.weight = 1.0f;
                    params.setMargins(2,2,2,2);
                    img.setLayoutParams(params);
                    img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    third.addView(img);
                    j++;
                }

                if(second.getChildCount() > 0){
                    if(holder.getItemViewType() == INCOMING){
                        ((InViewHolder) holder).imagesLayout.addView(second);
                    } else if(holder.getItemViewType() == OUT){
                        ((OutViewHolder) holder).imagesLayoutOut.addView(second);
                    }

                }


                if(third.getChildCount() > 0){
                    if(holder.getItemViewType() == INCOMING){
                        ((InViewHolder) holder).imagesLayout.addView(third);
                    } else if(holder.getItemViewType() == OUT){
                        ((OutViewHolder) holder).imagesLayoutOut.addView(third);
                    }
                }
            }


            //first.addView(second);
            //first.addView(third);
            //holder_in.imagesLayout.addView(first);




                    /*
                    <com.google.android.flexbox.FlexboxLayout
                    xmlns:android="http://schemas.android.com/apk/res/android"
                    xmlns:app="http://schemas.android.com/apk/res-auto"
                    android:id="@+id/dialogsFlexBoxLayoutIn"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    app:flexWrap="wrap"
                    app:alignItems="stretch"
                    app:alignContent="stretch"
                    app:showDivider="beginning|middle|end"
                    app:dividerDrawable="@drawable/divider"

                            >
                    </com.google.android.flexbox.FlexboxLayout>

                    for(int i = 0; i < messages.get(position).attachments.size(); i++){
                        if(messages.get(position).attachments.get(i).getType().equals(VKAttachments.TYPE_PHOTO)){
                            ImageView image = new ImageView(mContext);
                            image.setScaleType(ImageView.ScaleType.CENTER_CROP);

                            FlexboxLayout.LayoutParams lp;
                            VKApiPhoto photo = (VKApiPhoto) messages.get(position).attachments.get(i);

                            if(i == 0){
                                lp = new FlexboxLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                                lp.setFlexGrow(1);
                                Picasso.with(mContext).load(photo.photo_604).into(image);
                            } else {
                                lp = new FlexboxLayout.LayoutParams(300, 300);
                                Picasso.with(mContext).load(photo.photo_130).into(image);
                            }

                            if(i == 1){
                                //lp.setWrapBefore(true);
                            }

                            lp.setAlignSelf(AlignItems.STRETCH);

                            lp.setFlexGrow(1.0f);


                            holder_in.flexboxLayoutIn.addView(image, lp);
                            //holder.setIsRecyclable(false);
                            //TODO OVERRIDE onViewRecycled
                        }
                    }

                    */


        //returning position where another attachment type starts
        return j;
    }


    int arrangeImages(int position, ViewHolder holder){
        int j = 0;
        Log.d("ATTACHMENT", messages.get(position).attachments.get(0).getType());

        LinearLayout first = new LinearLayout(mContext);
        //LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams firstParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        first.setLayoutParams(firstParams);
        first.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams firstImageParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        firstImageParams.setMargins(2,2,2,2);
        ImageView firstImage = new ImageView(mContext);
        firstImage.setLayoutParams(firstImageParams);
        firstImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
        firstImage.setMinimumHeight(300);
        firstImage.setMinimumWidth(604);
        Picasso.with(mContext).load(((VKApiPhoto) messages.get(position).attachments.get(0)).photo_604).into(firstImage);
        first.addView(firstImage);
        //adding first image to first LinearLayout

        if(holder.getItemViewType() == INCOMING){
            ((InViewHolder) holder).imagesLayout.addView(first);
        } else if(holder.getItemViewType() == OUT){
            ((OutViewHolder) holder).imagesLayoutOut.addView(first);
        }




        LinearLayout second = new LinearLayout(mContext);
        LinearLayout third = new LinearLayout(mContext);


        //finding number of photo attachments
        int size = 0;
        for(int i = 0; i < messages.get(position).attachments.size(); i++){
            if(messages.get(position).attachments.get(i).getType().equals(VKAttachments.TYPE_PHOTO)){
                size++;
            }
        }



        if(size == 2){
            LinearLayout.LayoutParams secondParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            second.setLayoutParams(secondParams);
            second.setOrientation(LinearLayout.HORIZONTAL);
            ImageView img = new ImageView(mContext);

            LinearLayout.LayoutParams imgParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            imgParams.setMargins(2,2,2,2);

            img.setLayoutParams(imgParams);
            img.setMinimumHeight(300);
            img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Picasso.with(mContext).load(((VKApiPhoto) messages.get(position).attachments.get(1)).photo_604).into(img);
            second.addView(img);

            if(holder.getItemViewType() == INCOMING){
                ((InViewHolder) holder).imagesLayout.addView(second);
            } else if(holder.getItemViewType() == OUT){
                ((OutViewHolder) holder).imagesLayoutOut.addView(second);
            }
        } else if (size == 3){
            LinearLayout.LayoutParams secondParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            second.setLayoutParams(secondParams);
            second.setOrientation(LinearLayout.HORIZONTAL);
            second.setWeightSum(2);
            ImageView firstImageOnRow = new ImageView(mContext);
            ImageView secondImageOnRow = new ImageView(mContext);

            LinearLayout.LayoutParams imagesOnRowParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 200);
            imagesOnRowParams.weight = 1.0f;
            imagesOnRowParams.setMargins(2,2,2,2);

            firstImageOnRow.setLayoutParams(imagesOnRowParams);
            firstImageOnRow.setMinimumHeight(300);
            firstImageOnRow.setScaleType(ImageView.ScaleType.CENTER_CROP);

            Picasso.with(mContext).load(((VKApiPhoto) messages.get(position).attachments.get(1)).photo_604).into(firstImageOnRow);
            second.addView(firstImageOnRow);

            secondImageOnRow.setLayoutParams(imagesOnRowParams);
            secondImageOnRow.setMinimumHeight(300);
            secondImageOnRow.setScaleType(ImageView.ScaleType.CENTER_CROP);

            Picasso.with(mContext).load(((VKApiPhoto) messages.get(position).attachments.get(2)).photo_604).into(secondImageOnRow);
            second.addView(secondImageOnRow);

            if(holder.getItemViewType() == INCOMING){
                ((InViewHolder) holder).imagesLayout.addView(second);
            } else if(holder.getItemViewType() == OUT){
                ((OutViewHolder) holder).imagesLayoutOut.addView(second);
            }
        } else if(size > 3){

            int count2 = (size - 1) / 2;
            int count3 = (size - 1) - count2;

            int secondHeight = getImageHeight(count2);
            int thirdHeight = getImageHeight(count3);




            //LinearLayout.LayoutParams second_params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
            second.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, secondHeight));
            second.setOrientation(LinearLayout.HORIZONTAL);
            third.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, thirdHeight));
            third.setOrientation(LinearLayout.HORIZONTAL);

            second.setWeightSum(count2);
            third.setWeightSum(count3);


            //0  1 2 3 4   5 6 7 8 9


            j = 1;
            for(int i = 0; i < count2; i++){
                ImageView img = new ImageView(mContext);
                VKApiPhoto ph = (VKApiPhoto) messages.get(position).attachments.get(j);
                Picasso.with(mContext).load(ph.photo_604).into(img);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params.weight = 1.0f;
                params.setMargins(2,2,2,2);
                img.setLayoutParams(params);
                img.setScaleType(ImageView.ScaleType.CENTER_CROP);

                second.addView(img);
                j++;
            }
            for(int i = 0; i < count3; i++){
                ImageView img = new ImageView(mContext);
                VKApiPhoto ph = (VKApiPhoto) messages.get(position).attachments.get(j);
                Picasso.with(mContext).load(ph.photo_604).into(img);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params.weight = 1.0f;
                params.setMargins(2,2,2,2);
                img.setLayoutParams(params);
                img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                third.addView(img);
                j++;
            }

            if(second.getChildCount() > 0){
                if(holder.getItemViewType() == INCOMING){
                    ((InViewHolder) holder).imagesLayout.addView(second);
                } else if(holder.getItemViewType() == OUT){
                    ((OutViewHolder) holder).imagesLayoutOut.addView(second);
                }

            }


            if(third.getChildCount() > 0){
                if(holder.getItemViewType() == INCOMING){
                    ((InViewHolder) holder).imagesLayout.addView(third);
                } else if(holder.getItemViewType() == OUT){
                    ((OutViewHolder) holder).imagesLayoutOut.addView(third);
                }
            }
        }


        //first.addView(second);
        //first.addView(third);
        //holder_in.imagesLayout.addView(first);




                    /*
                    <com.google.android.flexbox.FlexboxLayout
                    xmlns:android="http://schemas.android.com/apk/res/android"
                    xmlns:app="http://schemas.android.com/apk/res-auto"
                    android:id="@+id/dialogsFlexBoxLayoutIn"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    app:flexWrap="wrap"
                    app:alignItems="stretch"
                    app:alignContent="stretch"
                    app:showDivider="beginning|middle|end"
                    app:dividerDrawable="@drawable/divider"

                            >
                    </com.google.android.flexbox.FlexboxLayout>

                    for(int i = 0; i < messages.get(position).attachments.size(); i++){
                        if(messages.get(position).attachments.get(i).getType().equals(VKAttachments.TYPE_PHOTO)){
                            ImageView image = new ImageView(mContext);
                            image.setScaleType(ImageView.ScaleType.CENTER_CROP);

                            FlexboxLayout.LayoutParams lp;
                            VKApiPhoto photo = (VKApiPhoto) messages.get(position).attachments.get(i);

                            if(i == 0){
                                lp = new FlexboxLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                                lp.setFlexGrow(1);
                                Picasso.with(mContext).load(photo.photo_604).into(image);
                            } else {
                                lp = new FlexboxLayout.LayoutParams(300, 300);
                                Picasso.with(mContext).load(photo.photo_130).into(image);
                            }

                            if(i == 1){
                                //lp.setWrapBefore(true);
                            }

                            lp.setAlignSelf(AlignItems.STRETCH);

                            lp.setFlexGrow(1.0f);


                            holder_in.flexboxLayoutIn.addView(image, lp);
                            //holder.setIsRecyclable(false);
                            //TODO OVERRIDE onViewRecycled
                        }
                    }

                    */


        //returning position where another attachment type starts
        return j;
    }


    private int getImageHeight(int count){
        switch (count){
            case 1:
                return 300;

            case 2:
                return 200;

            case 3:
                return 150;

            case 4:
                return 120;
            case 5:
                return 100;
            default:
                return 200;
        }

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        //if incoming message
        if(getItemViewType(position) == INCOMING) {
            InViewHolder holder_in = (InViewHolder) holder;
            holder_in.msgStatus.setVisibility(View.INVISIBLE);
            holder_in.imagesLayout.setClipToOutline(true);
            if(messages.getCount() > position){

                if(messages.get(position).body.equals("")){
                   // holder_in.message_in.setVisibility(View.GONE);
                } else {
                    holder_in.message_in.setVisibility(View.VISIBLE);
                    holder_in.message_in.setText(messages.get(position).body);
                }

                //TODO HERE
                holder_in.message_in.setText(messages.get(position).body);


                //String att =  messages.get(position).attachments.get(0).toAttachmentString().toString();
                 if(!(messages.get(position).attachments.isEmpty()) && messages.get(position).attachments.get(0).getType().equals(VKAttachments.TYPE_PHOTO)){
                    arrangeImages(position, holder);
                }

                if(messages.get(position).read_state){
                    holder_in.msgStatus.setVisibility(View.VISIBLE);
                    holder_in.msgStatus.setImageResource(R.drawable.read);
                }


                if (!messages.get(position).attachments.isEmpty() && messages.get(position).attachments.get(0).getType().equals(VKAttachments.TYPE_DOC)) {
                    if (((VKApiDocumentExtended) messages.get(position).attachments.get(0)).preview.audio != null) {

                        holder_in.message_in.setText("voice message");

                        VKApiAudioMsg audio_msg = ((VKApiDocumentExtended) messages.get(position).attachments.get(0)).preview.audio;

                        MediaPlayer mediaPlayer = new MediaPlayer();
                        try {
                            mediaPlayer.setDataSource(audio_msg.link_mp3);
                            Log.d("MSG", audio_msg.link_mp3);
                            mediaPlayer.prepare();
                            mediaPlayer.start();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }


            } else {
                holder_in.message_in.setText(messages_queue.get(position - messages.getCount()).body);
            }

            holder_in.msg_in_date.setText((new UnixTimeParser(mContext, messages.get(position).date)).getDateString());
        }

        //if out message
        if(getItemViewType(position) == OUT) {
            OutViewHolder holder_out = (OutViewHolder) holder;
            holder_out.imagesLayoutOut.setClipToOutline(true);

            if(messages.getCount() > position){
                holder_out.msg_out_date.setText((new UnixTimeParser(mContext, messages.get(position).date)).getDateString());

                if(messages.get(position).body.equals("")){
                    //holder_out.message_out.setVisibility(View.GONE);
                } else {
                    holder_out.message_out.setVisibility(View.VISIBLE);
                    holder_out.message_out.setText(messages.get(position).body);
                }


                if(!(messages.get(position).attachments.isEmpty()) && messages.get(position).attachments.get(0).getType().equals(VKAttachments.TYPE_PHOTO)){
                    arrangeImages(position, holder);
                }


                if (!messages.get(position).attachments.isEmpty() && messages.get(position).attachments.get(0).getType().equals(VKAttachments.TYPE_DOC)) {
                    if (((VKApiDocumentExtended) messages.get(position).attachments.get(0)).preview.audio != null) {

                        holder_out.message_out.setText("voice message");

                        VKApiAudioMsg audio_msg = ((VKApiDocumentExtended) messages.get(position).attachments.get(0)).preview.audio;
                        //VKApiDocument voiceMessage = (VKApiDocument) messages.get(position).attachments.get(0);

                        Log.d("MSG", "voice message on " + position + " position");

                        MediaPlayer mediaPlayer = new MediaPlayer();
                        try {
                            mediaPlayer.setDataSource(audio_msg.link_mp3);
                            Log.d("MSG", audio_msg.link_mp3);
                            mediaPlayer.prepare();
                            mediaPlayer.start();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }


                holder_out.msgStatus.setVisibility(View.VISIBLE);
                if(messages.get(position).read_state){
                    holder_out.msgStatus.setImageResource(R.drawable.read);
                } else {
                    holder_out.msgStatus.setImageResource(R.drawable.sent);
                }
            } else {
                holder_out.msg_out_date.setText((new UnixTimeParser(mContext, messages_queue.get(position - messages.getCount()).date)).getDateString());
                holder_out.message_out.setText(messages_queue.get(position - messages.getCount()).body);
                holder_out.msgStatus.setVisibility(View.INVISIBLE);
            }

        }

    }

    @Override
    public int getItemCount() {
        return messages.getCount() + messages_queue.getCount();
    }



    class ViewHolder extends RecyclerView.ViewHolder {




        public ViewHolder(View itemView) {
            super(itemView);

        }


    }

    class InViewHolder extends ViewHolder {

        final TextView message_in;
        final TextView msg_in_date;
        final ImageView msgStatus;
        //final FlexboxLayout flexboxLayoutIn;
        final LinearLayout imagesLayout;

        public InViewHolder(View itemView) {
            super(itemView);
            message_in = (TextView) itemView.findViewById(R.id.message_text_in);
            msg_in_date = (TextView) itemView.findViewById(R.id.msg_in_date);
            msgStatus = (ImageView) itemView.findViewById(R.id.msg_in_status);
            //flexboxLayoutIn = (FlexboxLayout) itemView.findViewById(R.id.dialogsFlexBoxLayoutIn);
            imagesLayout = (LinearLayout) itemView.findViewById(R.id.imagesLayout);

        }
    }

    class OutViewHolder extends ViewHolder {

        final TextView message_out;
        final ImageView msgStatus;
        final TextView msg_out_date;
        //final FlexboxLayout flexboxLayoutOut;
        final LinearLayout imagesLayoutOut;

        public OutViewHolder(View itemView){
            super(itemView);
            message_out = (TextView) itemView.findViewById(R.id.message_text_out);
            msgStatus = (ImageView) itemView.findViewById(R.id.msg_status);
            msg_out_date = (TextView) itemView.findViewById(R.id.msg_out_date);
            //flexboxLayoutOut = (FlexboxLayout) itemView.findViewById(R.id.dialogsFlexBoxLayoutOut);
            imagesLayoutOut = (LinearLayout) itemView.findViewById(R.id.imagesLayoutOut);
        }

    }

    class FooterViewHolder extends ViewHolder {

        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }




    @Override
    public int getItemViewType(int position) {



        if(messages.getCount() > position){
            if(messages.get(position).getClass() == MessagesFooter.class){
                return TYPE_FOOTER;
            }
            if (messages.get(position).out){
                return 1;
            } else
                return 0;
        } else {
            if(messages_queue.get(position - messages.getCount()).out) return 1;
                else return 0;
        }

    }

    public void updateAdapter(VKList<VKApiMessage> list){

        AdapterAsyncUpdate task = new AdapterAsyncUpdate();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, list);
        else
            task.execute(list);


    }

    public void updateAdapter(VKApiMessage msg){
        VKList<VKApiMessage> list = new VKList<>();
        list.add(msg);
        AdapterAsyncUpdate task = new AdapterAsyncUpdate();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, list);
        else
            task.execute(list);
    }

    public void addFooter(){
        if(messages.get(0).getClass() != MessagesFooter.class){
            MessagesFooter footer = new MessagesFooter();
            messages.add(0, footer);
        }

        notifyItemInserted(0);
    }

    public void removeFooter(){
        if(messages.get(0).getClass() == MessagesFooter.class)
            messages.remove(0);

        notifyItemRemoved(0);
    }


    private class AdapterAsyncUpdate extends AsyncTask<VKList<VKApiMessage>, Void, Void>{

        int size = 0;
        boolean footerRemoved = false;

        @SafeVarargs
        @Override
        protected final Void doInBackground(VKList<VKApiMessage>... params) {

            size = params[0].getCount();
            //removing footer
            if(messages.size() > 0 && messages.get(0).getClass() == MessagesFooter.class){
                messages.remove(0);
                footerRemoved = true;
            }
            for(int i = 0; i < size; i++){
                if(!messages.contains(params[0].get(i))){
                    Log.d("MSG", "DOESN'T CONTAIN");
                    messages.add(0, params[0].get(i));
                    //notifyItemInserted(0);
                } else {
                    Log.d("MSG", "CONTAIN");
                }
            }



            /*
            for(int i = (params[0].getCount() - 1); i >= 0; i--){
                Log.d("MSG", "COUNT IS" + params[0].getCount() + " " + i);

                if(!messages.contains(params[0].get(i))){
                    Log.d("MSG", "DOESN'T CONTAIN");
                    messages.add(params[0].get(i));
                } else {
                    Log.d("MSG", "CONTAIN");
                }

            }
            */


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(footerRemoved){
                notifyItemRemoved(0);
            }
            notifyItemRangeInserted(0, size);
            //notifyDataSetChanged();

        }
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);

        //Removing added views to use item later
        if(holder.getItemViewType() == OUT){
            //((OutViewHolder) holder).flexboxLayoutOut.removeAllViews();
            ((OutViewHolder) holder).imagesLayoutOut.removeAllViews();
        }

        if(holder.getItemViewType() == INCOMING){
            //((InViewHolder) holder).flexboxLayoutIn.removeAllViews();
            ((InViewHolder) holder).imagesLayout.removeAllViews();
        }

    }
}
