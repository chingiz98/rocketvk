package com.example.chingiz.vkchat.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import com.example.chingiz.vkchat.Adapters.PagerAdapter;
import com.example.chingiz.vkchat.Database.CacheDB;
import com.example.chingiz.vkchat.FirebaseInstanceIDService;
import com.example.chingiz.vkchat.Fragments.AllFriendsFragment;
import com.example.chingiz.vkchat.Fragments.DialogsFragment;
import com.example.chingiz.vkchat.Fragments.OnlineFriendsFragment;
import com.example.chingiz.vkchat.LongPoll.LongPollBroadcastService;
import com.example.chingiz.vkchat.MyApp;
import com.example.chingiz.vkchat.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKBatchRequest;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiCommunityArray;
import com.vk.sdk.api.model.VKApiDialog;
import com.vk.sdk.api.model.VKApiGetDialogResponse;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;
import com.vk.sdk.api.model.VKUsersArray;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements DialogsFragment.OnHeadlineSelectedListener, SwipeRefreshLayout.OnRefreshListener {


    private Toolbar toolbar;
    private boolean isResumed = false;


    public static final String USER_FIELDS = "id,first_name,last_name,bdate,online,photo_100,photo_max_orig";
    public static final String FRIENDS_FIELDS = "sex, bdate, city, country, photo_50, photo_100, photo_200_orig, photo_max_orig, contacts, education, online, relation, last_seen, status, universities";
    public static final int PICK_DIALOGS_BACK = 1;

    private TabLayout tabLayout;
    private boolean firstRun = true;

    private VKUsersArray friends;
    private VKUsersArray new_users;
    private static final String dialogs_count = "15";

    private DialogsFragment dialogs;
    private OnlineFriendsFragment online;
    private AllFriendsFragment all;
    private ViewPager viewPager;

    private ActionBar actionBar;
    private boolean isRegistered = false;

    SwipeRefreshLayout mSwipeRefreshDialogs;
    private SwipeRefreshLayout mainSwipeRefresh;

    // --Commented out by Inspection (29.10.16, 18:46):final String SENDER_ID = "654477674417";
    // --Commented out by Inspection (29.10.16, 18:46):final String REG_ID = "regid";

    private PagerAdapter adapter;

    private MyApp app;


// --Commented out by Inspection START (29.10.16, 18:46):
//    //GoogleCloudMessaging gcm;
//    String regId;
// --Commented out by Inspection STOP (29.10.16, 18:46)

    private static final int REQUEST_GOOGLE_PLAY_SERVICES = 1972;

    private static final String[] sMyScope = new String[]{
            VKScope.FRIENDS,
            VKScope.MESSAGES,
            VKScope.PHOTOS,
            VKScope.DOCS
    };



    BroadcastReceiver mainActUpdReceiver = new BroadcastReceiver() {
        final HashMap<Integer, CountDownTimer> timers_map = new HashMap<Integer, CountDownTimer>();

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();


            if(LongPollBroadcastService.ACTION_USER_TYPING_DIALOG.equals(action)){
                int user_id = intent.getIntExtra("id", 0);
                for(final VKApiDialog a : ((MyApp) getApplicationContext()).getDialogsList()){

                    if(a.message.user_id == user_id && !a.message.isChat){
                        //final String prev_body = a.message.body;

                        a.user_typing = true;
                        dialogs.notifyDataSetChanged();

                        if(timers_map.get(a.getId()) != null){
                            //Toast.makeText(MainActivity.this, "TIMER IS REMOVED", Toast.LENGTH_SHORT).show();
                            timers_map.get(a.getId()).cancel();
                            timers_map.remove(a.getId());
                        }

                        timers_map.put(a.getId(), new CountDownTimer(5500, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                //timers_map.remove(this.hashCode());
                                //a.message.body = prev_body;
                                //Toast.makeText(MainActivity.this, "TIMER IS FINISHED", Toast.LENGTH_SHORT).show();
                                a.user_typing = false;
                                //TODO REMOVE TIMER HERE
                                dialogs.notifyDataSetChanged();
                            }
                        }.start());

                        //Toast.makeText(MainActivity.this, "TIMER_MAP SIZE: " + timers_map.size(), Toast.LENGTH_SHORT).show();


                        break;
                    }
                }
            }

            if(LongPollBroadcastService.ACTION_MESSAGE_ADDED.equals(action)){
                Log.d("LONG", "MESSAGE ADDED IN MAIN");
                String photo_at = intent.getStringExtra("attachments");
                if(!photo_at.equals("")){
                    VKRequest req = new VKRequest("photos.getById", VKParameters.from("photos", photo_at));
                    req.executeWithListener(new VKRequest.VKRequestListener() {
                        @Override
                        public void onComplete(VKResponse response) {
                            super.onComplete(response);
                            Log.d("updates", "PHOTO_LOAD_OK " + response.responseString);

                        }

                        @Override
                        public void onError(VKError error) {
                            super.onError(error);
                            Log.d("updates", "PHOTO_LOAD_ERR " + error.errorReason + " " + error.toString());
                        }
                    });
                }

                //Toast.makeText(MainActivity.this, " MESSAGE ADDED", Toast.LENGTH_SHORT).show();
                int user_id = intent.getIntExtra("user_id", 0);
                int flags = intent.getIntExtra("flags", 0);
                String body = intent.getStringExtra("body");
                int id = intent.getIntExtra("id", 0);
                //if it is dialog message
                if((user_id / 2000000000) == 0){
                    VKList<VKApiDialog> list = ((MyApp) getApplicationContext()).getDialogsList();
                    for(int i = 0; i < list.size(); i++){
                        VKApiDialog cur_dialog = list.get(i);
                        if(cur_dialog.message.user_id == user_id && !cur_dialog.message.isChat){
                            //stopping timer if it exists
                            if(timers_map.get(cur_dialog.getId()) != null){
                                timers_map.get(cur_dialog.getId()).cancel();
                                timers_map.remove(cur_dialog.getId());
                            }

                            //inbox flag
                            if((flags & 0x0002) == 0){
                                cur_dialog.message.out = false;
                            } else {
                                cur_dialog.message.out = true;
                                cur_dialog.unread = 0;
                                cur_dialog.message.read_state = false;
                            }

                            cur_dialog.user_typing = false;
                            if(!cur_dialog.message.out)
                                cur_dialog.unread++;

                            cur_dialog.message.body = body;
                            cur_dialog.message.id = id;
                            list.remove(i);
                            list.add(0, cur_dialog);
                            dialogs.notifyDataSetChanged();



                            break;
                        }
                    }
                }
            }

            if(LongPollBroadcastService.ACTION_MESSAGE_FLAGS_RESET.equals(action)){
                //Toast.makeText(MainActivity.this, "FLAG RESET", Toast.LENGTH_SHORT).show();
                int user_id = intent.getIntExtra("user_id", 0);
                int flags = intent.getIntExtra("flags", 0);
                int message_id = intent.getIntExtra("message_id", 0);
                VKList<VKApiDialog> list = ((MyApp) getApplicationContext()).getDialogsList();
                //if in dialog message
                if((user_id / 2000000000) == 0){
                    for(VKApiDialog a : list){
                        if(a.message.user_id == user_id && !a.message.isChat){
                            //Reset unread flag
                            if((flags & 0x0001) == 1){
                                //Toast.makeText(MainActivity.this, "FLAG RESET BITWISE", Toast.LENGTH_SHORT).show();
                                a.message.read_state = !a.message.read_state;
                                a.unread = 0;
                            }
                            dialogs.notifyDataSetChanged();
                            break;
                        }
                    }
                }

            }
        }
    };





    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("MSG", "onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d("MSG", "onRestoreInstanceState");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("MSG", "onCreate is called");
        app = (MyApp) getApplicationContext();

        dialogs = new DialogsFragment();
        online = new OnlineFriendsFragment();
        all = new AllFriendsFragment();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //mainSwipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayoutMain);
        //mainSwipeRefresh.setOnRefreshListener(this);

        //mSwipeRefreshDialogs = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayoutDialogs);
        //stopService(new Intent(MainActivity.this, FirebaseInstanceIDService.class));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);


        setTabLayout();
        setViewPager();

        setViewPagerAdapter();

        if(!app.isInitialized()){
            offlineInitialize();
        }


        /*
        mSwipeRefreshDialogs.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateDialogs();
            }
        });

        */

        this.setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        //actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(false);

        wakeUpSession();
        //register();

    }





    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MSG", "onDestroy in MainActivity is called");
        unregisterReceiver(mainActUpdReceiver);
        stopService(new Intent(getApplicationContext(), LongPollBroadcastService.class));
        isRegistered = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("PAUSED");

        isResumed = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("RESUMED");
        if(!isRegistered){
            registerReceiver(mainActUpdReceiver, LongPollBroadcastService.makeIntentFilter());
        }

        if (isResumed) {
            System.out.println("WAKING UP");
            //startService(new Intent(getApplicationContext(), LongPollBroadcastService.class));
            wakeUpSession();
        }

        //TODO FIX THIS
        try{
            //updateDialogs();
            dialogs.notifyDataSetChanged();
        } catch (Exception e){

        }


    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean offlineInitialize(){
        CacheDB cache_db = new CacheDB(this);

        VKUsersArray friends_from_db = cache_db.getFriends();

        final VKList<VKApiDialog> dialogs_from_db = cache_db.getDialogs();
        VKUsersArray not_friends = cache_db.getUsers();

        ((MyApp) getApplicationContext()).addFriends(friends_from_db);
        //((MyApp) getApplicationContext()).setOnlineFriends(online_friends_json);
        ((MyApp) getApplicationContext()).addDialogs(dialogs_from_db);
        //((MyApp) getApplicationContext()).setUsers(friends);
        ((MyApp) getApplicationContext()).addNewUsers(not_friends);


        //not_friends = ((MyApp) getApplicationContext()).getNewUsers();

        online.online = false;

        //setViewPagerAdapter();
        return true;
    }

    private void setTabLayout(){
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.dialogs_tab)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.online_tab)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.all_friends_tab)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    private void setViewPager(){
        viewPager.setOffscreenPageLimit(3);

        //SwipeRefreshLayout disable on right/left page  swipe
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        //mSwipeRefreshLayout.setEnabled(false);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        // mSwipeRefreshLayout.setEnabled(true);
                        break;
                }
                return false;
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

      /*  viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                enableDisableSwipeRefresh(state == ViewPager.SCROLL_STATE_IDLE);
            }
        });

*/
    }

    private void enableDisableSwipeRefresh(boolean enable) {
        if (mainSwipeRefresh != null) {
            mainSwipeRefresh.setEnabled(enable);
        }
    }

    private void setViewPagerAdapter(){

        adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), dialogs, online, all);
        viewPager.setAdapter(adapter);


    }

    private boolean networkInitialize() {



        final CacheDB cache_db = new CacheDB(this);
        //final UsersDB mDbHelper_friends = new UsersDB(this);

        final SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("data", MODE_PRIVATE);


        final boolean firstRun = sharedPref.getBoolean("isFirstRun", true);
        //boolean firstRun = true;

        VKRequest request_friends = VKApi.friends().get(VKParameters.from(VKApiConst.FIELDS, FRIENDS_FIELDS, "order", "name"));
        VKRequest request_onlineFriends = VKApi.friends().getOnline(VKParameters.from("order", "hints"));
        VKRequest request_dialogs = VKApi.messages().getDialogs(VKParameters.from(VKApiConst.COUNT, dialogs_count));
        final VKRequest request_currentUser = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, FRIENDS_FIELDS));



        //final ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);


        if (firstRun) {
            //mDbHelper_friends.addUsers();
            final VKBatchRequest batch = new VKBatchRequest(request_friends, request_onlineFriends, request_dialogs, request_currentUser);
            //startService(new Intent(getApplicationContext(), LongPollBroadcastService.class));

            batch.executeWithListener(new VKBatchRequest.VKBatchRequestListener() {
                @Override
                public void onComplete(VKResponse[] responses) {
                    super.onComplete(responses);
                    SharedPreferences.Editor editor = sharedPref.edit();

                    VKApiGetDialogResponse msg = (VKApiGetDialogResponse) responses[2].parsedModel;
                    final VKList<VKApiDialog> dialogs_list = msg.items;


                    VKUsersArray current_users_resp = new VKUsersArray();
                    current_users_resp.addAll((VKList<VKApiUserFull>) responses[3].parsedModel);
                    cache_db.addUsers(current_users_resp, false);

                    //VKApiUser current_user = ((VKUsersArray) responses[3].parsedModel).get(0);


                    friends = (VKUsersArray) responses[0].parsedModel;


                    //VKUsersArray users = (VKUsersArray) responses[0].parsedModel;
                    cache_db.addUsers((VKUsersArray) responses[0].parsedModel, true);


                    JSONArray online_friends_json = null;
                    try {
                        online_friends_json = responses[1].json.getJSONArray("response");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    Log.d("MSG", "response count: " + responses[1].json.toString());
                    //Log.d("MSG", "ONLINE FRIENDS COUNT: " + online_friends.getCount());

                    boolean thereIsNewUsers = false;
                    boolean thereIsNewGroupsUsers = false;
                    String new_array_users = "";
                    String new_groups_users = ""; //for chats with communities

                    new_users = new VKUsersArray();
                    new_users.add(current_users_resp.get(0)); //adding current user

                    int current_user_id = Integer.parseInt(VKAccessToken.currentToken().userId); //Current AccessToken owner


                    //Adding new users that are not in friends to database
                    for (VKApiDialog dialog : dialogs_list) {
                        if (friends.getById(dialog.message.user_id) == null
                                && current_user_id != dialog.message.user_id) { //if not in usersList and not current User
                                if(dialog.message.user_id < 0){
                                    new_groups_users = new_groups_users + Math.abs(dialog.message.user_id) + ",";
                                    thereIsNewGroupsUsers = true;
                                }


                            new_array_users = new_array_users + dialog.message.user_id + ",";
                            thereIsNewUsers = true;

                        }
                    }


                    try {
                        ((MyApp) getApplicationContext()).addOnlineFriends(online_friends_json);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    ((MyApp) getApplicationContext()).addDialogs(dialogs_list);
                    ((MyApp) getApplicationContext()).addNewUsers(new_users);
                    ((MyApp) getApplicationContext()).addFriends(friends);




                    if (thereIsNewUsers) {
                        updateDialogsListWithNewUsers(new_array_users);
                    }

                    if(thereIsNewGroupsUsers){
                        updateDialogsListWithNewGroups(new_groups_users);
                    }

                    //Ending part

                    Log.d("MSG", "Dialogs list size in MainActivity is: " + ((MyApp) getApplicationContext()).getDialogsList().size());

                    dialogs.notifyDataSetChanged();
                    all.notifyDataSetChanged();
                    online.notifyDataSetChanged();



                    cache_db.addUsers(friends, true);
                    cache_db.addOrUpdateDialogs(dialogs_list);

                    editor.putBoolean("isFirstRun", false);
                    editor.apply();


                }
            });

            actionBar.setTitle(R.string.rocket_vk);

            return true;

        } else {
            updateFriends();
            return updateDialogs();
        }


        //final SwipeRefreshLayout mSwipeRefreshLayout = (cRefreshLayout) findViewById(R.id.swipe_container);


    }

    public void updateFriends(){
        VKRequest request_friends = VKApi.friends().get(VKParameters.from(VKApiConst.FIELDS, FRIENDS_FIELDS, "order", "name"));
        request_friends.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                VKUsersArray friends_upd = (VKUsersArray) response.parsedModel;
                CacheDB db = new CacheDB(MainActivity.this);
                db.addUsers(friends_upd, true);

                ((MyApp) getApplicationContext()).setFriends(friends_upd);

                all.notifyDataSetChanged();


            }
        });

    }

    public boolean updateDialogs(){
        //TODO MAKE NON_FRIENDS UPDATE IN FUTURE
        final CacheDB cache_db = new CacheDB(this);
        final VKList<VKApiDialog> dialogs_from_db = ((MyApp) getApplicationContext()).getDialogsList();
        VKRequest request_onlineFriends = VKApi.friends().getOnline(VKParameters.from("order", "hints"));
        VKRequest request_dialogs = VKApi.messages().getDialogs(VKParameters.from(VKApiConst.COUNT, dialogs_count));


        final VKBatchRequest batch = new VKBatchRequest(request_dialogs, request_onlineFriends);


        batch.executeWithListener(new VKBatchRequest.VKBatchRequestListener() {
            @Override
            public void onComplete(VKResponse[] responses) {
                //mSwipeRefreshDialogs = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayoutDialogs);

                JSONArray online_friends_json = null;
                try {
                    online_friends_json = responses[1].json.getJSONArray("response");
                    ((MyApp) getApplicationContext()).addOnlineFriends(online_friends_json);
                    ((MyApp) getApplicationContext()).updateFriendsListWithOnline(online_friends_json);
                    online.notifyDataSetChanged();
                    //all.notifyItemRangeChanged();
                    all.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                VKApiGetDialogResponse msg_update = (VKApiGetDialogResponse) responses[0].parsedModel;
                final VKList<VKApiDialog> dialogs_for_update = msg_update.items;
                cache_db.addOrUpdateDialogs(dialogs_for_update);

                //if there is no last message from server list in database then update...
                int end = dialogs_for_update.size();

                for(int i = 0; i < dialogs_for_update.size(); i++){
                    VKApiDialog check_dialog = dialogs_from_db.getById(dialogs_for_update.get(i).message.id);
                    if(check_dialog != null) {
                        end = i;
                        break;
                    }
                }

                if (end != dialogs_for_update.size()) { //fix

                    Log.d("MSG", "NEED UPDATE");

                    //Removing older dialogs.
                    for (int i = 0; i < end; i++) {
                        VKApiDialog cur_uptodate_dialog = dialogs_for_update.get(i);
                        int chat_id = cur_uptodate_dialog.message.chat_id;
                        //boolean no_matches = true;
                        //if (cur_uptodate_dialog.message.isChat)
                        //    chat_id = cur_uptodate_dialog.message.chat_id;
                        for (int j = 0; j < dialogs_from_db.size(); j++) {
                            if ((dialogs_from_db.get(j).message.user_id == cur_uptodate_dialog.message.user_id
                                    && dialogs_from_db.get(j).message.chat_id == 0) || (dialogs_from_db.get(j).message.chat_id == chat_id && dialogs_from_db.get(j).message.chat_id != 0)) {
                                dialogs_from_db.remove(j);
                                //no_matches = false;
                            }
                        }

                    }

                    boolean thereIsNewUsers = false;
                    boolean thereIsNewGroupsUsers = false;
                    String new_users_req_str = "";
                    String new_groups_users = ""; //for chats with communities

                    //Adding new dialogs
                    for (int i = end - 1; i >= 0; i--){
                        VKApiDialog dialogToAdd = dialogs_for_update.get(i);
                        dialogs_from_db.add(0, dialogToAdd);
                        if((((MyApp) getApplicationContext()).getUsers().getById(dialogToAdd.message.user_id)) == null)
                            thereIsNewUsers = true;
                        new_users_req_str += "," + dialogToAdd.message.user_id;
                        if(dialogToAdd.message.user_id < 0){
                            new_groups_users = new_groups_users + Math.abs(dialogToAdd.message.user_id) + ",";
                            thereIsNewGroupsUsers = true;
                        }

                    }



                    if (thereIsNewUsers){
                        updateDialogsListWithNewUsers(new_users_req_str);
                    } else {
                        dialogs.notifyDataSetChanged(); //TODO HERE
                    }

                    if(thereIsNewGroupsUsers){
                        updateDialogsListWithNewGroups(new_groups_users);
                    }




                    //dialogs.setSwipeRefreshLayoutRefreshing(false);

                } else {
                    //TODO DROP TABLE HERE
                }
                actionBar.setTitle(R.string.rocket_vk);
            }
        });



        dialogs.notifyDataSetChanged();
        online.notifyDataSetChanged();



        return true;
    }

    private void updateDialogsListWithNewUsers(String new_array_users) {
        final CacheDB cache_db = new CacheDB(this);
        VKRequest get_user = VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS, new_array_users, VKApiConst.FIELDS, USER_FIELDS, "order", "name"));
        get_user.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                //TODO FIX THE BUGS OVER HERE!
                //VKList<VKApiUserFull> users_resp = (VKList<VKApiUserFull>) response.parsedModel;
                VKUsersArray new_users_resp = new VKUsersArray();
                new_users_resp.addAll((VKList<VKApiUserFull>) response.parsedModel);
                ((MyApp) getApplicationContext()).getNewUsers().addAll(new_users_resp);
                //                              dialogs.recycler.getAdapter().notifyDataSetChanged();
                cache_db.addUsers(new_users_resp, false);
                dialogs.notifyDataSetChanged();
            }

        });
    }

    private void updateDialogsListWithNewGroups(String new_groups_users){
        final CacheDB cache_db = new CacheDB(this);
        VKRequest get_groups = VKApi.groups().getById(VKParameters.from(VKApiConst.GROUP_IDS, new_groups_users));

        get_groups.executeWithListener(new VKRequest.VKRequestListener() {

            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                VKApiCommunityArray groupsArray = (VKApiCommunityArray) response.parsedModel;
                VKUsersArray groupsToUsers = new VKUsersArray();

                for(int i = 0; i < groupsArray.size(); i++){
                    VKApiUserFull tmp = new VKApiUserFull();
                    tmp.first_name = groupsArray.get(i).name;
                    tmp.last_name = "";
                    tmp.photo_50 = groupsArray.get(i).photo_50;
                    tmp.photo_100 = groupsArray.get(i).photo_100;
                    tmp.photo_200 = groupsArray.get(i).photo_200;
                    tmp.id = -groupsArray.get(i).id;
                    tmp.online = false;
                    //TODO ADD screen_name field
                    groupsToUsers.add(tmp);
                }

                ((MyApp) getApplicationContext()).getNewUsers().addAll(groupsToUsers);
                cache_db.addUsers(groupsToUsers, false);
                dialogs.notifyDataSetChanged();

            }
        });
    }




    private void startRegistrationService() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int code = api.isGooglePlayServicesAvailable(this);
        if (code == ConnectionResult.SUCCESS) {
            onActivityResult(REQUEST_GOOGLE_PLAY_SERVICES, Activity.RESULT_OK, null); //If play services is ok, will go to the next stage
        } else if (api.isUserResolvableError(code) &&
                api.showErrorDialogFragment(this, code, REQUEST_GOOGLE_PLAY_SERVICES)) {
            // wait for onActivityResult call (see below)
        } else {
            Toast.makeText(this, api.getErrorString(code), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode == Activity.RESULT_OK) {
                    startService(new Intent(this, FirebaseInstanceIDService.class)); // OK, init GCM if it not registered
                    //Toast.makeText(this, "GCM SERVICE STARTED", Toast.LENGTH_LONG).show();
                }
                break;
            case PICK_DIALOGS_BACK:
                Log.d("MSG", "PICK_DIALOGS_BACK");
                isResumed = false;
                if(data.getBooleanExtra(DialogActivity.UPDATE_FLAG, false)){
                    dialogs.notifyDataSetChanged();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }

        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                // Пользователь успешно авторизовался
            }

            @Override
            public void onError(VKError error) {
                // Произошла ошибка авторизации (например, пользователь запретил авторизацию)
            }
        })) {

            super.onActivityResult(requestCode, resultCode, data);
        }

    }


    private void wakeUpSession(){
    /*    if(!isOfflineInitialized){
            offlineInitialize();
            isOfflineInitialized = true;
        }
*/
        VKSdk.wakeUpSession(this, new VKCallback<VKSdk.LoginState>() {
            @Override
            public void onResult(VKSdk.LoginState res) {

                switch (res) {
                    case LoggedOut:
                        Log.d("MSG", "CASE LOGGED OUT");
                        VKSdk.login(MainActivity.this, sMyScope);

                        break;
                    case LoggedIn:
                        Log.d("MSG", "isInitialized is " + app.isInitialized());

                            actionBar.setTitle(R.string.updating);
                            Log.d("MSG", "CASE LOGGED IN");
                            startRegistrationService();
                            Log.d("MSG", "TOKEN IS " + FirebaseInstanceId.getInstance().getToken());

                            networkInitialize();

                        app.setInitialized(true);

                        isResumed = false;
                        break;
                    case Pending:
                        Log.d("MSG", "PENDING...");
                        //actionBar.setTitle(R.string.connecting);
                        break;
                    case Unknown:
                        Log.d("MSG", "UNKNOWN");
                        break;
                }


            }

            @Override
            public void onError(VKError error) {

            }
        });

    }




    @Override
    public void onRefresh() {
        Toast.makeText(MainActivity.this, "REFRESHING_FROM MAIN", Toast.LENGTH_SHORT).show();
        updateDialogs();
        //firstInit(15);
    }


}


