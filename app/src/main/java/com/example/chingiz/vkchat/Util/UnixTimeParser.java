package com.example.chingiz.vkchat.Util;

/**
 * Created by chingiz on 20.03.2017.
 */
import android.content.Context;
import com.example.chingiz.vkchat.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author God
 */
public class UnixTimeParser {

    private int year;
    private int second;
    private int minute;
    private int hour;
    private int day;
    private int month;
    private boolean leap = false;
    private long unix_time = 0;
    private static int[] month_days = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    private static String[] month_names = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private Context mContext;


    public UnixTimeParser(Context context, long unix_time){
        mContext = context;
        this.unix_time = unix_time;
        convertToDate();
    }

    public UnixTimeParser(Context context, int year, int month, int day, int hour, int minute, int second){
        mContext = context;
        this.year = year;
        this.month = month;
        if((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) //проверка года на високосность
            leap = true;
        if(!leap && month == 2 && day > 28){
            System.out.println("INCORRECT DATE");
            return;
        }
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        unix_time = convertToUnix();
    }


    void print(){
        System.out.printf("%d %s %d %d:%d:%d\n", year, month_names[month - 1], day, hour, minute, second);

    }

    private void convertToDate(){ //перевод unix_time в обычную форму




        year = (int) (unix_time / 31536000) + 1970;
        int leap_years = (year - 1968) / 4;
        long all_days = unix_time / 86400;

        long days_no_leap = all_days - leap_years;

        int this_year_days = (int) (days_no_leap % 365) + 1;
        int this_month_days = this_year_days;

        if((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)){ //проверка года на високосность
            leap = true;
        }


        for(int i = 0; i < 12; i++){
            if(this_month_days <= month_days[i] || (leap && i == 1 && this_month_days <= month_days[i] + 1)) {
                month = i + 1;
                day = (int) this_month_days;
                break;
            }
            this_month_days -= month_days[i];
        }

        int current_day_sec = (int) (unix_time - all_days * 86400);
        hour = current_day_sec / 3600; //часов в текущем дне

        minute = (current_day_sec - (hour * 3600)) / 60; //минут в текущем часе
        second = (current_day_sec - (hour * 3600)) - (minute * 60); //секунд в текущей минуте


    }
    private long convertToUnix(){

        long unix_t = (year - 1970) * 31536000;
        int leap_days = (year - 1969) / 4;
        unix_t += leap_days * 86400;

        if((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)){ //проверка года на високосность
            if(month > 2 || month == 2 && day > 28)
                unix_t += 86400;
        }

        for(int i = 0; i < month - 1; i++){
            unix_t += 86400 * month_days[i];
        }

        unix_t += (day - 1) * 86400;

        if(day == 29 && month == 2){
            unix_t -= 86400;
        }

        unix_t += hour * 3600;
        unix_t += minute * 60;
        unix_t += second;

        return unix_t;

    }
    private long getHNY(int year){

        long unix_t = (year - 1970) * 31536000;
        int leap_days = (year - 1968) / 4;
        unix_t += leap_days * 86400;


        return unix_t;

    }

    public String getDateString(){

        Date currentDate = new Date();
        long now_day = currentDate.getTime();
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        long now_hours = calendar.get(Calendar.HOUR_OF_DAY);
        long now_minutes = calendar.get(Calendar.MINUTE);
        long now_seconds = calendar.get(Calendar.SECOND);
        long midnight = (now_day / 1000) - (now_hours * 3600) - (now_minutes * 60) - now_seconds;
        long new_year = getHNY(calendar.get(Calendar.YEAR));
        SimpleDateFormat dateFormat;
        String dateString = String.valueOf(unix_time);

        int DAY = 84600;
        if(unix_time >= midnight){
            dateFormat = new SimpleDateFormat("H:mm");
            dateFormat.setTimeZone(TimeZone.getDefault());
            dateString = dateFormat.format(unix_time * 1000);
        }
        else if(unix_time >= (midnight - DAY)&&(unix_time < midnight)){
            dateFormat = new SimpleDateFormat("H:mm");
            dateFormat.setTimeZone(TimeZone.getDefault());
            dateString = mContext.getResources().getString(R.string.yesterday) + " " + dateFormat.format(unix_time * 1000);
        }

        else if((unix_time >= new_year) && (unix_time < midnight)){
            dateFormat = new SimpleDateFormat("dd MMMM");
            dateFormat.setTimeZone(TimeZone.getDefault());
            dateString = dateFormat.format(unix_time * 1000);
        }
        else if(unix_time < new_year){
            dateFormat = new SimpleDateFormat("dd MMMM yyyy");
            dateFormat.setTimeZone(TimeZone.getDefault());
            dateString = dateFormat.format(unix_time * 1000);
        }

        return dateString;
    }
    public long getUnixTimeStamp(){
        return unix_time;
    }

}