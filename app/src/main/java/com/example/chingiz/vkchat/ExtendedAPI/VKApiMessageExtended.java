package com.example.chingiz.vkchat.ExtendedAPI;

import com.vk.sdk.api.model.VKApiMessage;
import com.vk.sdk.api.model.VKAttachments;

import org.json.JSONException;
import org.json.JSONObject;

public class VKApiMessageExtended extends VKApiMessage {




    @Override
    public VKApiMessage parse(JSONObject source) throws JSONException {
        super.parse(source);
        attachments = new VKAttachmentsExtended();
        attachments.fill(source.optJSONArray("attachments"));

        return this;
    }
}
