package com.example.chingiz.vkchat.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.chingiz.vkchat.JSON.JsonConstants;
import com.vk.sdk.api.model.VKApiUser;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by chingiz on 03.10.2015.
 */
@SuppressWarnings("unused")
class MessagesDB {





    //public static final String RECORD_CHECK = "Select * from " + TableName + " where " + dbfield + " = " + fieldValue;
    private static final String TEXT = " TEXT,";
    private static final String INTEGER = " INTEGER,";
    private static final String SQL_CREATE_ENTRIES_MESSAGES =
            "CREATE TABLE " + MessagesCacheContract.MessagesCacheEntry.TABLE_NAME + " (" +
                    MessagesCacheContract.MessagesCacheEntry._ID + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.TITLE + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.BODY + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.MSG_ID + TEXT +
                    //MessagesCacheContract.MessagesCacheEntry.FROM_ID + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.USER_ID + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.DATE + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.FIRST_NAME + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.LAST_NAME + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.READ_STATE + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.OUT + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.GEO + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.ATTACHMENTS + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.FWD_MESSAGES + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.EMOJI + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.IMPORTANT + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.DELETED + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.CHAT_ID + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.CHAT_ACTIVE + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PUSH_SETTINGS + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.USERS_COUNT + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.ADMIN_ID + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.ACTION + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.ACTION_EMAIL + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.ACTION_TEXT + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PHOTO_50_CHAT + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PHOTO_50_USER + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PHOTO_100_CHAT + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PHOTO_100_USER + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PHOTO_CHAT + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PHOTO_200_USER + " TEXT" +
// Any other options for the CREATE command
                    " );";


    private static final String[] projection_messages = {
            MessagesCacheContract.MessagesCacheEntry._ID,
            MessagesCacheContract.MessagesCacheEntry.FIRST_NAME,
            MessagesCacheContract.MessagesCacheEntry.LAST_NAME,
            MessagesCacheContract.MessagesCacheEntry.MSG_ID,
            MessagesCacheContract.MessagesCacheEntry.USER_ID,
            MessagesCacheContract.MessagesCacheEntry.DATE,
            MessagesCacheContract.MessagesCacheEntry.READ_STATE,
            MessagesCacheContract.MessagesCacheEntry.OUT,
            MessagesCacheContract.MessagesCacheEntry.TITLE,
            MessagesCacheContract.MessagesCacheEntry.BODY,
            MessagesCacheContract.MessagesCacheEntry.GEO,
            MessagesCacheContract.MessagesCacheEntry.ATTACHMENTS,
            MessagesCacheContract.MessagesCacheEntry.FWD_MESSAGES,
            MessagesCacheContract.MessagesCacheEntry.EMOJI,
            MessagesCacheContract.MessagesCacheEntry.IMPORTANT,
            MessagesCacheContract.MessagesCacheEntry.DELETED,
            MessagesCacheContract.MessagesCacheEntry.CHAT_ID,
            MessagesCacheContract.MessagesCacheEntry.CHAT_ACTIVE,
            MessagesCacheContract.MessagesCacheEntry.PUSH_SETTINGS,
            MessagesCacheContract.MessagesCacheEntry.USERS_COUNT,
            MessagesCacheContract.MessagesCacheEntry.ADMIN_ID,
            MessagesCacheContract.MessagesCacheEntry.ACTION,
            MessagesCacheContract.MessagesCacheEntry.ACTION_EMAIL,
            MessagesCacheContract.MessagesCacheEntry.ACTION_TEXT,
            MessagesCacheContract.MessagesCacheEntry.PHOTO_50_CHAT,
            MessagesCacheContract.MessagesCacheEntry.PHOTO_50_USER,
            MessagesCacheContract.MessagesCacheEntry.PHOTO_100_CHAT,
            MessagesCacheContract.MessagesCacheEntry.PHOTO_100_USER,
            MessagesCacheContract.MessagesCacheEntry.PHOTO_CHAT,
            MessagesCacheContract.MessagesCacheEntry.PHOTO_200_USER

    };

    private static final String SQL_DELETE_ENTRIES_MESSAGES =
            "DROP TABLE IF EXISTS " + MessagesCacheContract.MessagesCacheEntry.TABLE_NAME;


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "MessagesCache.db";

    private MessagesCacheDbHelper DBHelper;
    private SQLiteDatabase db;

// --Commented out by Inspection START (29.10.16, 18:30):
//    public MessagesDB(Context context){
//        DBHelper = new MessagesCacheDbHelper(context);
//    }
// --Commented out by Inspection STOP (29.10.16, 18:30)

    @SuppressWarnings("unused")
    private static class MessagesCacheDbHelper extends SQLiteOpenHelper {

        public MessagesCacheDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES_MESSAGES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(SQL_DELETE_ENTRIES_MESSAGES);
            onCreate(db);
        }


        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }

    }

    private void open() throws SQLiteException {
        db = DBHelper.getWritableDatabase();
    }

    //closes the database
    private void close(){
        DBHelper.close();
    }





    public void addDialogs(JSONObject json_msg, VKApiUser usr) {

        open();

        try {

            String checkRecordQuery;


            System.out.println("JSON HAS CHAT_ID1 IS " + json_msg.has(JsonConstants.CHAT_ID));

            if (json_msg.has(JsonConstants.CHAT_ID)) {
                checkRecordQuery = "Select * from " + MessagesCacheContract.MessagesCacheEntry.TABLE_NAME +
                        " where " +
                        MessagesCacheContract.MessagesCacheEntry.CHAT_ID + " = " + json_msg.getString(JsonConstants.CHAT_ID);

            } else {
                checkRecordQuery = "Select * from " + MessagesCacheContract.MessagesCacheEntry.TABLE_NAME +
                        " where " + MessagesCacheContract.MessagesCacheEntry.USER_ID + " = '" + json_msg.getString(JsonConstants.USER_ID) + "' AND " +
                        MessagesCacheContract.MessagesCacheEntry.CHAT_ID + " = '0'";

            }
            Cursor cursor = db.rawQuery(checkRecordQuery, null);


            System.out.println("CURSOR COUNT IS " + cursor.getCount());

            if (cursor.getCount() <= 0) {
                ContentValues values = new ContentValues();
                values.clear();


                values.put(MessagesCacheContract.MessagesCacheEntry.MSG_ID, json_msg.getString(JsonConstants.ID));

                values.put(MessagesCacheContract.MessagesCacheEntry.USER_ID, json_msg.getString(JsonConstants.USER_ID));
                values.put(MessagesCacheContract.MessagesCacheEntry.DATE, json_msg.getString(JsonConstants.DATE));

                if (usr != null) {
                    values.put(MessagesCacheContract.MessagesCacheEntry.FIRST_NAME, usr.first_name);
                    values.put(MessagesCacheContract.MessagesCacheEntry.LAST_NAME, usr.last_name);
                }

                values.put(MessagesCacheContract.MessagesCacheEntry.READ_STATE, json_msg.getString(JsonConstants.READ_STATE));
                values.put(MessagesCacheContract.MessagesCacheEntry.OUT, json_msg.getString(JsonConstants.OUT));
                values.put(MessagesCacheContract.MessagesCacheEntry.TITLE, json_msg.getString(JsonConstants.TITLE));
                values.put(MessagesCacheContract.MessagesCacheEntry.BODY, json_msg.getString(JsonConstants.BODY));

                if (json_msg.has(JsonConstants.EMOJI)) {
                    values.put(MessagesCacheContract.MessagesCacheEntry.EMOJI, json_msg.getString(JsonConstants.EMOJI));
                }


                if (json_msg.has(JsonConstants.CHAT_ID)) {

                    System.out.println("MSG BEFORE ADDING PHOTO OF CHAT: " + json_msg.toString());

                    values.put(MessagesCacheContract.MessagesCacheEntry.CHAT_ID, json_msg.getString(JsonConstants.CHAT_ID));
                    if (usr != null)
                        values.put(MessagesCacheContract.MessagesCacheEntry.PHOTO_100_USER, usr.photo_100);
                    try {
                        values.put(MessagesCacheContract.MessagesCacheEntry.PHOTO_100_CHAT, json_msg.getString(JsonConstants.PHOTO_100));
                    } catch (Exception ignored) {

                    }


                } else {
                    values.put(MessagesCacheContract.MessagesCacheEntry.CHAT_ID, 0);
                }


                if (!json_msg.has(JsonConstants.CHAT_ID)) {
                    if (usr != null)
                        values.put(MessagesCacheContract.MessagesCacheEntry.PHOTO_100_USER, usr.photo_100);
                }

                if (usr != null) System.out.println("DIALOG PHOTO IS " + usr.photo_100);


                db.insert(MessagesCacheContract.MessagesCacheEntry.TABLE_NAME, null, values);
            } else {

                System.out.println("JSON HAS CHAT_ID IS " + json_msg.has(JsonConstants.CHAT_ID));

                if (json_msg.has(JsonConstants.CHAT_ID)) {
                    //chat update

                    System.out.println("CHAT UPDATE TRIGGER");

                    ContentValues values = new ContentValues();
                    values.clear();
                    values.put(MessagesCacheContract.MessagesCacheEntry.MSG_ID, json_msg.getString(JsonConstants.ID));

                    values.put(MessagesCacheContract.MessagesCacheEntry.USER_ID, json_msg.getString(JsonConstants.USER_ID));
                    values.put(MessagesCacheContract.MessagesCacheEntry.DATE, json_msg.getString(JsonConstants.DATE));
                    if (usr != null) {
                        values.put(MessagesCacheContract.MessagesCacheEntry.FIRST_NAME, usr.first_name);
                        values.put(MessagesCacheContract.MessagesCacheEntry.LAST_NAME, usr.last_name);
                    }
                    values.put(MessagesCacheContract.MessagesCacheEntry.READ_STATE, json_msg.getString(JsonConstants.READ_STATE));
                    values.put(MessagesCacheContract.MessagesCacheEntry.OUT, json_msg.getString(JsonConstants.OUT));
                    values.put(MessagesCacheContract.MessagesCacheEntry.TITLE, json_msg.getString(JsonConstants.TITLE));
                    values.put(MessagesCacheContract.MessagesCacheEntry.BODY, json_msg.getString(JsonConstants.BODY));


                    if (usr != null) {
                        values.put(MessagesCacheContract.MessagesCacheEntry.PHOTO_100_USER, usr.photo_100);
                    } else {
                        values.put(MessagesCacheContract.MessagesCacheEntry.PHOTO_100_CHAT, json_msg.getString(JsonConstants.PHOTO_100));
                    }


                    db.update(MessagesCacheContract.MessagesCacheEntry.TABLE_NAME, values, MessagesCacheContract.MessagesCacheEntry.CHAT_ID + " = " + json_msg.getString(JsonConstants.CHAT_ID), null);

                } else {
                    //dialog update

                    System.out.println("DIALOG UPDATE TRIGGER");

                    ContentValues values = new ContentValues();
                    values.clear();

                    values.put(MessagesCacheContract.MessagesCacheEntry.MSG_ID, json_msg.getString(JsonConstants.ID));

                    values.put(MessagesCacheContract.MessagesCacheEntry.USER_ID, json_msg.getString(JsonConstants.USER_ID));
                    values.put(MessagesCacheContract.MessagesCacheEntry.DATE, json_msg.getString(JsonConstants.DATE));
                    if (usr != null) {
                        values.put(MessagesCacheContract.MessagesCacheEntry.FIRST_NAME, usr.first_name);
                        values.put(MessagesCacheContract.MessagesCacheEntry.LAST_NAME, usr.last_name);
                    }
                    values.put(MessagesCacheContract.MessagesCacheEntry.READ_STATE, json_msg.getString(JsonConstants.READ_STATE));
                    values.put(MessagesCacheContract.MessagesCacheEntry.OUT, json_msg.getString(JsonConstants.OUT));
                    values.put(MessagesCacheContract.MessagesCacheEntry.TITLE, json_msg.getString(JsonConstants.TITLE));
                    values.put(MessagesCacheContract.MessagesCacheEntry.BODY, json_msg.getString(JsonConstants.BODY));

                    if (usr != null) {
                        System.out.println("DIALOG PHOTO IS " + usr.photo_100);
                        values.put(MessagesCacheContract.MessagesCacheEntry.PHOTO_100_USER, usr.photo_100);
                        System.out.println("PHOTO UPDATED");
                    }


                    db.update(MessagesCacheContract.MessagesCacheEntry.TABLE_NAME, values, MessagesCacheContract.MessagesCacheEntry.USER_ID + " = " + json_msg.getString(JsonConstants.USER_ID) + " AND " +
                            MessagesCacheContract.MessagesCacheEntry.CHAT_ID + " = '0'", null);
                }

            }

            cursor.close();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        close();


    }

    public VKApiUser getUserInfoOfDialog(int user_id) {

        open();


        Cursor cur = db.query(true, MessagesCacheContract.MessagesCacheEntry.TABLE_NAME, projection_messages,
                MessagesCacheContract.MessagesCacheEntry.USER_ID + "=?",
                new String[]{String.valueOf(user_id)},
                null, null, null, null);

        VKApiUser user = new VKApiUser();

        cur.moveToFirst();

        user.first_name = cur.getString(cur.getColumnIndex(MessagesCacheContract.MessagesCacheEntry.FIRST_NAME));
        user.last_name = cur.getString(cur.getColumnIndex(MessagesCacheContract.MessagesCacheEntry.LAST_NAME));
        //user.online = cur.getString(cur.getColumnIndex(cur.))
        user.photo_100 = cur.getString(cur.getColumnIndex(MessagesCacheContract.MessagesCacheEntry.PHOTO_100_USER));

        close();
        cur.close();

        return user;
    }

}
