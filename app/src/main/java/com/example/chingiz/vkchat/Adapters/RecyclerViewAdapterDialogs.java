package com.example.chingiz.vkchat.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chingiz.vkchat.Activities.DialogActivity;
import com.example.chingiz.vkchat.Activities.MainActivity;
import com.example.chingiz.vkchat.Activities.ProfileActivity;
import com.example.chingiz.vkchat.Interfaces.ItemClickListener;
import com.example.chingiz.vkchat.MyApp;
import com.example.chingiz.vkchat.R;
import com.example.chingiz.vkchat.Util.UnixTimeParser;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiDialog;
import com.vk.sdk.api.model.VKApiMessage;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;
import com.vk.sdk.api.model.VKUsersArray;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Admin on 13.09.2015.
 */
public class RecyclerViewAdapterDialogs extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    //public VKList<VKApiDialog> dialogs;
    //public VKList<VKApiUser> users;
    //public JSONObject dialogs_json;
    //private String current_user_photo_url;
    private VKList<VKApiDialog> dialogsList;
    private final VKUsersArray users;
    private VKUsersArray new_users = new VKUsersArray();
    private static int border_width;
    private static final int TYPE_DIALOG = 0;
    private static final int TYPE_FOOTER = 1;
    private static final String EMPTY = "";





    public RecyclerViewAdapterDialogs(Context context, VKList<VKApiDialog> dialogsList, VKUsersArray users, VKUsersArray new_users) {
        Log.d("MSG", "RecyclerViewAdapterDialogs dialogsList size in constructor: " + dialogsList.size());
        Log.d("MSG", "RecyclerViewAdapterDialogs users size in constructor: " + users.size());
        mContext = context;
        this.dialogsList = dialogsList;
        this.users = users;
        this.new_users = new_users;


       /* //TODO DELETE THIS!!!
        for(int i = 0; i < dialogsList.size(); i++){
            if(dialogsList.get(i).message.isChat){
                dialogsList.remove(i);
            }
        }
*/
    }

    @Override
    public int getItemViewType(int position) {
        Log.d("MSG", "INSTANCE OF  AT POSITION " + position);
        if (dialogsList.get(position).getClass() == VKApiDialog.class) {
            Log.d("MSG", "INSTANCE OF DIALOG AT POSITION " + position);
            return TYPE_DIALOG;
        } else if (dialogsList.get(position).getClass() == Footer.class) {
            Log.d("MSG", "INSTANCE OF FOOTER");
            return TYPE_FOOTER;
        } else {
            throw new RuntimeException("ItemViewType unknown");
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

       if(viewType == TYPE_DIALOG){
           View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dialogs_item, viewGroup, false);
           Log.d("MSG!!!!!!!!!!!", "VIEWHOLDER CREATED");
           return new DialogViewHolder(v);
       } else {
           Log.d("MSG", "footer viewholder created");
           View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.progress_footer, viewGroup, false);
           return new FooterViewHolder(v);
       }





    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int i) {

        if (viewHolder.getItemViewType() == TYPE_DIALOG) {

            if(users.getById(dialogsList.get(i).message.user_id) != null){
                initializeViewHolder(viewHolder, i, dialogsList.get(i), users.getById(dialogsList.get(i).message.user_id));

            } else if(new_users.getById(dialogsList.get(i).message.user_id) != null ){
                initializeViewHolder(viewHolder, i, dialogsList.get(i), new_users.getById(dialogsList.get(i).message.user_id));
            } else {

                //ADDITIONAL USER CHECK
                VKApiUser usr = new VKApiUser();
                usr.first_name = "SES";
                usr.last_name = "KOK";
                //initializeViewHolder(viewHolder, i, dialogsList.get(i).message, usr);
                VKRequest get_user = VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS, dialogsList.get(i).message.user_id, VKApiConst.FIELDS, "photo_100"));
                get_user.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {

                        if(((VKList<VKApiUserFull>) response.parsedModel).size() > 0){

                            new_users.add(((VKList<VKApiUserFull>) response.parsedModel).get(0));
                            RecyclerViewAdapterDialogs.this.notifyDataSetChanged();
                            initializeViewHolder(viewHolder, i, dialogsList.get(i), ((VKList<VKApiUserFull>) response.parsedModel).get(0));
                        }

                    }

                });

            }
            Log.d("MSG", "MESSAGE IS: " + dialogsList.get(i).message.isChat + " " + dialogsList.get(i).message.chat_id + " " + dialogsList.get(i).message.body  + " " + dialogsList.get(i).message.title + " " + dialogsList.get(i).message.user_id);


        }




    }

    private void initializeViewHolder(RecyclerView.ViewHolder viewHolder, final int i, final VKApiDialog dlg, final VKApiUser usr){
        final DialogViewHolder vh = (DialogViewHolder) viewHolder;
        final VKApiMessage msg = dlg.message;

        vh.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {

                VKApiUser usr = new VKApiUser();

                if(users.getById(dialogsList.get(i).message.user_id) != null){
                    usr = users.getById(dialogsList.get(i).message.user_id);
                } else if(new_users.getById(dialogsList.get(i).message.user_id) != null){
                    usr = new_users.getById(dialogsList.get(i).message.user_id);
                } else {
                    usr = null;
                }

                if (!isLongClick) {

                    if(usr != null){
                        ((MainActivity) mContext).startActivityForResult(new Intent(mContext, DialogActivity.class).putExtra("dialog", dlg).putExtra("user", usr), MainActivity.PICK_DIALOGS_BACK);

                    } else {

                        Toast.makeText(mContext, "can't get user", Toast.LENGTH_SHORT).show();

                    }

                } else {

                    //Toast.makeText(mContext, "#" + position + " - (Long click) total count is: " + dialogsList.getCount(), Toast.LENGTH_SHORT).show();

                }
            }
        });

        //opening profile activity by pressing on user's picture
        vh.pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileActivityIntent = new Intent(mContext, ProfileActivity.class);
                profileActivityIntent.putExtra("user", usr);
                mContext.startActivity(profileActivityIntent);
            }
        });



        //if (vh.pic.getBorderWidth() != 0) border_width = vh.pic.getBorderWidth();

        //vh.pic.setBorderWidth(0);

        Picasso pic = Picasso.with(mContext);
        System.out.println("CURRENT ITEM IS " + i);
        vh.pic.setBorderWidth(0);
        vh.msgStatus.setVisibility(View.INVISIBLE);

        if (msg.isChat) {
            vh.unreadCountBackgroud.setVisibility(View.INVISIBLE);
            vh.unreadCount.setVisibility(View.INVISIBLE);


            try {
                Drawable myDrawable;
                Log.d("MSG", "CHAT PHOTO FIELDS IS: " + msg.photo_100_chat);
                if(!(msg.photo_100_chat.equals(EMPTY))){

                        pic.load(msg.photo_100_chat).into(vh.pic);

                } else {
                    myDrawable = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.account_multiple, null);
                    vh.pic.setImageDrawable(myDrawable);
                }

                //System.out.println(dialogs.get(i).message.body + " IS SET IMAGE ");
                vh.name.setText(msg.title);



                vh.last_msg.setText(msg.body);


                vh.date.setText((new UnixTimeParser(mContext, msg.date)).getDateString());


            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }


        } else {

            vh.unreadCountBackgroud.setVisibility(View.INVISIBLE);
            vh.unreadCount.setVisibility(View.INVISIBLE);



            try {

                //Log.d("MSG", new_users.getById(msg.user_id).first_name + " STATUS IS " + new_users.getById(msg.user_id).online);

                if(users.getById(msg.user_id) != null){
                    if(users.getById(msg.user_id).online)
                        vh.pic.setBorderWidth(3); //8
                } else {
                    if(new_users.getById(msg.user_id).online)
                        vh.pic.setBorderWidth(3); //8
                }

            } catch (Exception ignored) {
                ignored.printStackTrace();
            }

            try {
                Log.d("MSG", "PICTURE IS: " + usr.photo_100);
                Picasso.with(mContext).load(usr.photo_100).into(vh.pic);
                //System.out.println(dialogs.get(i).message.body + " IS SET IMAGE ");
                //System.out.println(users.get(i).first_name + " IS SET THIS IMAGE");

            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }


            if(dlg.user_typing){
                vh.last_msg.setText(mContext.getString(R.string.typing));
            } else {
                vh.last_msg.setText(msg.body);
            }




            vh.date.setText((new UnixTimeParser(mContext, msg.date)).getDateString());

            //System.out.println("FRIEND NAME PART");
            try {
                String full_name = usr.first_name + " " + usr.last_name;
                vh.name.setText(full_name);
            } catch (Exception ignored){

            }

            if(msg.out){
                vh.msgStatus.setVisibility(View.VISIBLE);
                if(msg.read_state){
                    vh.msgStatus.setImageResource(R.drawable.read);
                } else {
                    vh.msgStatus.setImageResource(R.drawable.sent);
                }
            } else {
                //TODO SHOW INCOMING MESSAGES COUNT HERE
                if(dlg.unread > 0){
                    vh.unreadCountBackgroud.setVisibility(View.VISIBLE);
                    vh.unreadCount.setVisibility(View.VISIBLE);
                    vh.unreadCount.setText(Integer.toString(dlg.unread));
                }
            }


        }
    }


    @Override
    public int getItemCount() {

        if (dialogsList != null) {
            VKUsersArray tmp = ((MyApp) mContext.getApplicationContext()).getFriends();
            VKUsersArray tmp1 = users;
            Log.d("MSG", "RecyclerViewAdapterDialogs size of global users list is: " + ((MyApp) mContext.getApplicationContext()).getFriends().size());
            Log.d("MSG", "RecyclerViewAdapterDialogs size of users list is: " + users.size());
            Log.d("MSG", "RecyclerViewAdapterDialogs size of global dialogs list is: " + ((MyApp) mContext.getApplicationContext()).getDialogsList().size());
            Log.d("MSG", "RecyclerViewAdapterDialogs getItemCount is called: " + dialogsList.size());
            return dialogsList.size();
        }
        return 0;
    }

    public static class DialogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private final TextView name;
        private final TextView last_msg;
        final CircleImageView pic;
        private final CircleImageView last_msg_pic;
        private final ImageView msgStatus;
        private TextView date;
        private TextView unreadCount;
        private FrameLayout unreadCountBackgroud;
        private CountDownTimer timer;
        //private CircleImageView user_pic;
        //ImageView online_indicator;

        private ItemClickListener clickListener;

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        public DialogViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.from_name);
            pic = (CircleImageView) itemView.findViewById(R.id.dialogs_user_pic);
            last_msg_pic = (CircleImageView) itemView.findViewById(R.id.last_msg_pic);
            last_msg = (TextView) itemView.findViewById(R.id.last_msg);
            //online_indicator = (ImageView) itemView.findViewById(R.id.imageOnline_Dialog);
            msgStatus = (ImageView) itemView.findViewById(R.id.msgStatus);
            date = (TextView) itemView.findViewById(R.id.date_of_message);
            //user_pic = (CircleImageView) itemView.findViewById(R.id.dialogs_user_pic);
            unreadCount = (TextView) itemView.findViewById(R.id.unread_count);
            unreadCountBackgroud = (FrameLayout) itemView.findViewById(R.id.unread_count_background);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);


        }


        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onClick(v, getAdapterPosition(), true);
            return true;
        }


    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {



        public FooterViewHolder(View itemView) {
            super(itemView);

        }
    }
}


