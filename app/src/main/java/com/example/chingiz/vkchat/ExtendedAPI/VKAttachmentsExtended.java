package com.example.chingiz.vkchat.ExtendedAPI;

import com.vk.sdk.api.model.VKApiApplicationContent;
import com.vk.sdk.api.model.VKApiAudio;
import com.vk.sdk.api.model.VKApiDocument;
import com.vk.sdk.api.model.VKApiLink;
import com.vk.sdk.api.model.VKApiNote;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKApiPhotoAlbum;
import com.vk.sdk.api.model.VKApiPoll;
import com.vk.sdk.api.model.VKApiPost;
import com.vk.sdk.api.model.VKApiPostedPhoto;
import com.vk.sdk.api.model.VKApiVideo;
import com.vk.sdk.api.model.VKApiWikiPage;
import com.vk.sdk.api.model.VKAttachments;

import org.json.JSONArray;
import org.json.JSONObject;

public class VKAttachmentsExtended extends VKAttachments {

    public VKAttachmentsExtended(){

    }

    @Override
    public void fill(JSONObject from) {
        super.fill(from, parser);
    }

    @Override
    public void fill(JSONArray from) {
        super.fill(from, parser);
    }

    private final Parser<VKApiAttachment> parser = new Parser<VKApiAttachment>() {
        @Override
        public VKApiAttachment parseObject(JSONObject attachment) throws Exception {
            String type = attachment.optString("type");
            if(TYPE_PHOTO.equals(type)) {
                return new VKApiPhoto().parse(attachment.getJSONObject(TYPE_PHOTO));
            } else if(TYPE_VIDEO.equals(type)) {
                return new VKApiVideo().parse(attachment.getJSONObject(TYPE_VIDEO));
            } else if(TYPE_AUDIO.equals(type)) {
                return new VKApiAudio().parse(attachment.getJSONObject(TYPE_AUDIO));
            } else if(TYPE_DOC.equals(type)) {
                return new VKApiDocumentExtended().parse(attachment.getJSONObject(TYPE_DOC));
            } else if(TYPE_POST.equals(type)) {
                return new VKApiPost().parse(attachment.getJSONObject(TYPE_POST));
            } else if(TYPE_POSTED_PHOTO.equals(type)) {
                return new VKApiPostedPhoto().parse(attachment.getJSONObject(TYPE_POSTED_PHOTO));
            } else if(TYPE_LINK.equals(type)) {
                return new VKApiLink().parse(attachment.getJSONObject(TYPE_LINK));
            } else if(TYPE_NOTE.equals(type)) {
                return new VKApiNote().parse(attachment.getJSONObject(TYPE_NOTE));
            } else if(TYPE_APP.equals(type)) {
                return new VKApiApplicationContent().parse(attachment.getJSONObject(TYPE_APP));
            } else if(TYPE_POLL.equals(type)) {
                return new VKApiPoll().parse(attachment.getJSONObject(TYPE_POLL));
            } else if(TYPE_WIKI_PAGE.equals(type)) {
                return new VKApiWikiPage().parse(attachment.getJSONObject(TYPE_WIKI_PAGE));
            } else if(TYPE_ALBUM.equals(type)) {
                return new VKApiPhotoAlbum().parse(attachment.getJSONObject(TYPE_ALBUM));
            }
            return null;
        }
    };
}
