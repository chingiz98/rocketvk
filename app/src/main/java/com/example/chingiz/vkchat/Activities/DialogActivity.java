package com.example.chingiz.vkchat.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Px;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.example.chingiz.vkchat.Adapters.AttachmentsCards.AttachmentCard;
import com.example.chingiz.vkchat.Adapters.AttachmentsCards.AttachmentCardPhoto;
import com.example.chingiz.vkchat.Adapters.AttachmentsPanelAdapter;
import com.example.chingiz.vkchat.Adapters.RVMessagesAdapter;
//import com.example.chingiz.vkchat.Database.DialogsCacheDbHelper;
import com.example.chingiz.vkchat.Database.CacheDB;
import com.example.chingiz.vkchat.ExtendedAPI.VKApiMessageExtended;
import com.example.chingiz.vkchat.LongPoll.LongPollBroadcastService;
import com.example.chingiz.vkchat.MyApp;
import com.example.chingiz.vkchat.R;
import com.squareup.picasso.Picasso;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiImageView;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.emoji.Emoji;
import com.vanniktech.emoji.listeners.OnEmojiBackspaceClickListener;
import com.vanniktech.emoji.listeners.OnEmojiClickListener;
import com.vanniktech.emoji.listeners.OnEmojiPopupDismissListener;
import com.vanniktech.emoji.listeners.OnEmojiPopupShownListener;
import com.vanniktech.emoji.listeners.OnSoftKeyboardCloseListener;
import com.vanniktech.emoji.listeners.OnSoftKeyboardOpenListener;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiChat;
import com.vk.sdk.api.model.VKApiDialog;
import com.vk.sdk.api.model.VKApiGetMessagesResponse;
import com.vk.sdk.api.model.VKApiMessage;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;

/**
 * Created by chingiz on 11.04.2016.
 */
@SuppressWarnings("FieldCanBeLocal")
public class DialogActivity extends AppCompatActivity {

    private final int COUNT = 200;
    private final int USER_ID = 0;
    private boolean hidden = true;

    private static final int RC_CODE_PICKER = 2000;
    private static final int RC_CAMERA = 3000;
    private ArrayList<Image> images = new ArrayList<>();

    private VKApiUser user;
    private VKApiDialog dialog;

    private CacheDB msgs_db;
    private RVMessagesAdapter messagesAdapter;
    private RecyclerView dialogRecyclerView;
    private TextView friendName;

    private RecyclerView mAttachmentsRecyclerview;
    private RecyclerView.Adapter mAttachmentsAdapter;
    private RecyclerView.LayoutManager mAttachmentsLayoutManager;
    private ArrayList<AttachmentCard> mAttachmentsDataSet;

    private CircleImageView friendImage;
//    private LinearLayout mRevealView;
    //private ImageView attach;
    private ImageView send;
    private EmojiEditText textBoard;
    private EmojIconActions actions;
    private ImageView emojiButton;
    private ImageView attachButton;
    private BottomSheetBehavior mBottomSheetBehavior;

    private View imagePicker;

    private EmojiPopup emojiPopup;

    private VKList<VKApiMessage> messages;
    private VKList<VKApiMessage> messages_queue;

    private TextView friend_status;
    private boolean updateMain = false;
    public static final String UPDATE_FLAG = "need_update";
    private boolean isChat = false;
    private boolean isInitialized = false;
    private boolean isInBackground = false;
    private boolean dialogsLoading = false;

    //private ArrayList<VKRequest> photoUploadRequests;
    //private ArrayList<String> photoAttachments;
    //private String[] photoAttachments = {"", "", "", "", "", "", "", "", "", ""};




    @Override
    protected void onResume() {
        super.onResume();
        isInBackground = false;
        registerReceiver(mUpdateReceiver, LongPollBroadcastService.makeIntentFilter());
        Log.d("DIALOG_A", "ON RESUME");
    }


    @Override
    protected void onPause() {
        super.onPause();
        //unregisterReceiver(mUpdateReceiver);
        Log.d("DIALOG_A", "ON PAUSE");
        isInBackground = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mUpdateReceiver);
        Log.d("DIALOG_A", "ON DESTROY");
    }

    private void notifyOnDialogThread(){
        messagesAdapter.notifyItemInserted(messagesAdapter.getItemCount());
        dialogRecyclerView.smoothScrollToPosition(messagesAdapter.getItemCount());
        //Toast.makeText(this, "notify", Toast.LENGTH_SHORT).show();
    }


    /*
    private void clearPhotoAttachments(){
        for (int i = 0; i < photoAttachments.length; i++) {
            photoAttachments[i] = "";
        }
    }

    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.dialog_activity);

        ImageView backButton = findViewById(R.id.backButtonDialogs);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        View bottomSheet = findViewById( R.id.bottom_sheet );
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(0);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        msgs_db = new CacheDB(this);
        messages = msgs_db.getMessages(USER_ID);

        messages_queue = new VKList<VKApiMessage>();


        dialogRecyclerView = (RecyclerView) findViewById(R.id.recycler_dialog);

        mAttachmentsRecyclerview = (RecyclerView) findViewById(R.id.dialogs_attachments_recycler);
        mAttachmentsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mAttachmentsRecyclerview.setLayoutManager(mAttachmentsLayoutManager);
        mAttachmentsDataSet = new ArrayList<>();

        mAttachmentsAdapter = new AttachmentsPanelAdapter(this, mAttachmentsDataSet, images);
        mAttachmentsRecyclerview.setAdapter(mAttachmentsAdapter);

        //photoUploadRequests = new ArrayList<>();
        //photoAttachments = new ArrayList<>(10);
        //photoAttachments = new HashMap<Integer, String>();

        final LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
        dialogRecyclerView.setLayoutManager(mLinearLayoutManager);



        dialogRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                //hiding attachments panel
                if(mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);


                // Get the first visible item
                int firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
                if(firstVisibleItem == 0 && !dialogsLoading){
                    dialogsLoading = true;

                    messagesAdapter.addFooter();

                    final VKRequest request = new VKRequest("messages.getHistory", VKParameters.from(VKApiConst.COUNT, COUNT, VKApiConst.OFFSET, 1, VKApiConst.USER_ID, user.id, VKApiConst.START_MESSAGE_ID, messages.get(1).id));
                    request.executeWithListener(new VKRequest.VKRequestListener() {
                        @Override
                        public void onComplete(VKResponse response) {
                            super.onComplete(response);
                            //messagesAdapter.removeFooter();
                            VKApiGetMessagesResponse msg_response = new VKApiGetMessagesResponse(response.json);



                            final VKList<VKApiMessage> messages = msg_response.items;



                            Log.d("MSG_RESPONSE", "SIZE " + messages.size());

                            messagesAdapter.updateAdapter(messages);
                            //messagesAdapter.setHasStableIds(true);
                            //dialogsLoading = false;
                            if(messages.size() > 0){
                                dialogsLoading = false;
                            } else {
                                //if we reached last message preventing future loadings
                                dialogsLoading = true;
                            }


                        }
                    });
                    //Toast.makeText(DialogActivity.this, "TOP", Toast.LENGTH_SHORT).show();
                }



            }
        });




        emojiButton = (ImageView) findViewById(R.id.imageView17);
        emojiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emojiPopup.toggle();
            }
        });

        attachButton = (ImageView) findViewById(R.id.imageView16);
        attachButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startImagePicker();
                //mBottomSheetBehavior.setPeekHeight(200);
                //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });


        imagePicker = (View) findViewById(R.id.imagePicker);
        imagePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startImagePicker();
            }
        });

        textBoard = (EmojiEditText) findViewById(R.id.textBoard);
        Intent intent = getIntent();


        //actions = new EmojIconActions(getApplicationContext(), (LinearLayout) findViewById(R.id.keyboard_listener), textBoard, emojiButton);
        //actions.ShowEmojIcon();

        setUpEmojiPopup();

        user = intent.getParcelableExtra("user");
        dialog = intent.getParcelableExtra("dialog");


        friendName = (TextView) findViewById(R.id.friendName);
        friendImage = (CircleImageView) findViewById(R.id.friendAvatar);
        friendImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileActivityIntent = new Intent(DialogActivity.this, ProfileActivity.class);
                profileActivityIntent.putExtra("user", user);
                startActivity(profileActivityIntent);
            }
        });

        friend_status = (TextView) findViewById(R.id.friendStatus);





        setStatus();




        send = (ImageView) findViewById(R.id.sendButton);
        send.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                boolean sendFlag = true;
                for (int i = 0; i < mAttachmentsDataSet.size(); i++) {
                    if(mAttachmentsDataSet.get(i).isUploading())
                        sendFlag = false;
                }


                if(sendFlag){
                    //TODO IMPLEMENT MESSAGES QUEUE
                    final String REQUEST = "messages.send";
                    String photos = "";
                    for(int i = 0; i < mAttachmentsDataSet.size(); i++){
                        //if(!photoAttachments[i].equals(""))
                        photos += mAttachmentsDataSet.get(i).getURI() + ",";
                    }

                    if(!textBoard.getText().toString().equals("") || !photos.equals("")){
                        final int pos = messages_queue.getCount();

                        VKApiMessage queued_msg = new VKApiMessage();
                        queued_msg.body = textBoard.getText().toString();
                        queued_msg.out = true;
                        queued_msg.date = System.currentTimeMillis() / 1000;
                        queued_msg.user_id = user.id;
                        messages_queue.add(queued_msg);


                        notifyOnDialogThread();
                        //messagesAdapter.notifyItemInserted(messagesAdapter.getItemCount());

                        VKRequest req = new VKRequest(REQUEST, VKParameters.from("user_id", user.id, "message", textBoard.getText().toString(), "attachment", photos));
                        textBoard.setText("");
                        req.executeWithListener(new VKRequest.VKRequestListener() {

                            @Override
                            public void onComplete(VKResponse response) {
                                VKApiMessage cur = messages_queue.get(pos);

                                super.onComplete(response);

                                cur.id = response.json.optInt("response");
                                cur.read_state = false;
                                messages.add(cur);

                                messages_queue.remove(pos);
                                messagesAdapter.notifyDataSetChanged();

                                mAttachmentsDataSet.clear();
                                mAttachmentsAdapter.notifyDataSetChanged();
                                //photoAttachments.clear();
                                //clearPhotoAttachments();


                            }
                        });
                    }

                } else {
                    Toast.makeText(DialogActivity.this, getString(R.string.attachments_still_loading), Toast.LENGTH_SHORT).show();
                }



            }
        });

        /*attach = (ImageView) findViewById(R.id.attachButton);

        attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(DialogActivity.this, "attach clicked", Toast.LENGTH_SHORT).show();
                int cx = (attach.getLeft() + attach.getRight())/2;
//                int cy = (mRevealView.getTop() + mRevealView.getBottom())/2;
                int cy = mRevealView.getBottom();

                int radius = Math.max(mRevealView.getWidth(), mRevealView.getHeight());

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {


                } else {
                    if (hidden) {
                        Animator anim = android.view.ViewAnimationUtils.createCircularReveal(mRevealView, cx, cy, 0, radius);
                        mRevealView.setVisibility(View.VISIBLE);
                        anim.start();
                        hidden = false;

                    } else {
                        Animator anim = android.view.ViewAnimationUtils.createCircularReveal(mRevealView, cx, cy, radius, 0);
                        anim.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                mRevealView.setVisibility(View.INVISIBLE);
                                hidden = true;
                            }
                        });
                        anim.start();

                    }
                }
            }
        });*/

        /*mRevealView = (LinearLayout) findViewById(R.id.reveal_items);
        mRevealView.setVisibility(View.INVISIBLE);*/





        Log.d("DialogActivity", "" + user.id);
        //Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

        //TODO: Change this shit after recreating dialogs cache logic!!!
        //MessagesCacheDbHelper dbHelper = new MessagesCacheDbHelper(this);
        //VKApiUser user = dbHelper.getUserInfoOfDialog(USER_ID);
        String friend_name = user.first_name + " " + user.last_name;
        friendName.setText(friend_name);
        Picasso.with(this).load(user.photo_100).into(friendImage);

        //


        setMainRecyclerView();




        final VKRequest request = new VKRequest("messages.getHistory", VKParameters.from("count", COUNT, "user_id", user.id));

        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {

                //VKApiGetMessagesResponse msg_response = (VKApiGetMessagesResponse) response.parsedModel;





                VKApiGetMessagesResponse msg_response = new VKApiGetMessagesResponse(response.json){
                    @Override
                    public VKApiGetMessagesResponse parse(JSONObject source) {
                        JSONObject response = source.optJSONObject("response");
                        this.count = response.optInt("count");
                        this.items = new VKList<VKApiMessage>(response.optJSONArray("items"), VKApiMessageExtended.class);
                        this.unread = response.optInt("unread");
                        return this;
                    }
                };


                Log.d("MSG_RESPONSE", response.json.toString());
                Log.d("MSG_FIELDS", "" + msg_response.items.get(0).body);
                final VKList<VKApiMessage> messages = msg_response.items;

                messagesAdapter.updateAdapter(messages);
                //messagesAdapter.setHasStableIds(true);
                isInitialized = true;
                //SETTING READ STATE
                if(msg_response.unread > 0){
                    updateMain = true;
                    markAsRead();
                }


                AsyncMessagesAdd task = new AsyncMessagesAdd();



                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, messages);
                else
                    task.execute(messages);




            }
        });


        textBoard.addTextChangedListener(new TextWatcher() {
            long last_time = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Log.d("TEXT", "BEFORE");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Log.d("TEXT", "ON CHANGE");
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("TEXT", "AFTER CHANGE");
                if(System.currentTimeMillis() - last_time > 6000 || last_time == 0){
                    Log.d("TEXT", "EXECUTED");
                    last_time = System.currentTimeMillis();
                    VKRequest typing_req = new VKRequest("messages.setActivity", VKParameters.from("user_id", user.id, "type", "typing"));
                    typing_req.executeWithListener(new VKRequest.VKRequestListener() {
                        @Override
                        public void onComplete(VKResponse response) {
                            super.onComplete(response);
                        }
                    });
                }
            }
        });

    }


    private void setUpEmojiPopup() {
        emojiPopup = EmojiPopup.Builder.fromRootView((ViewGroup) findViewById(R.id.keyboard_listener))
                .setOnEmojiBackspaceClickListener(new OnEmojiBackspaceClickListener() {
                    @Override public void onEmojiBackspaceClick(final View v) {
                        Log.d("MSG", "Clicked on Backspace");
                    }
                })
                .setOnEmojiClickListener(new OnEmojiClickListener() {
                    @Override public void onEmojiClick(@NonNull final EmojiImageView imageView, @NonNull final Emoji emoji) {
                        Log.d("MSG", "Clicked on emoji");
                    }
                })
                .setOnEmojiPopupShownListener(new OnEmojiPopupShownListener() {
                    @Override public void onEmojiPopupShown() {
                        emojiButton.setImageResource(R.drawable.ic_keyboard);
                    }
                })
                .setOnSoftKeyboardOpenListener(new OnSoftKeyboardOpenListener() {
                    @Override public void onKeyboardOpen(@Px final int keyBoardHeight) {
                        Log.d("MSG", "Opened soft keyboard");
                    }
                })
                .setOnEmojiPopupDismissListener(new OnEmojiPopupDismissListener() {
                    @Override public void onEmojiPopupDismiss() {
                        emojiButton.setImageResource(R.drawable.emoticon);
                    }
                })
                .setOnSoftKeyboardCloseListener(new OnSoftKeyboardCloseListener() {
                    @Override public void onKeyboardClose() {
                        Log.d("MSG", "Closed soft keyboard");
                    }
                })
                .build(textBoard);
    }


    public void startImagePicker() {
        /*
        getPhotosUploadServerRequest = VKApi.photos().getMessagesUploadServer();
        getPhotosUploadServerRequest.attempts = 10;
        getPhotosUploadServerRequest.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                upload_url = response.json.optJSONObject("response").optString("upload_url");
                album_id = response.json.optJSONObject("response").optString("album_id");
                Toast.makeText(DialogActivity.this, "upload url is " + upload_url + " album id is " + album_id, Toast.LENGTH_SHORT).show();
            }
        });
        */

        //final boolean returnAfterCapture = ((Switch) findViewById(R.id.ef_switch_return_after_capture)).isChecked();
        //final boolean isSingleMode = ((Switch) findViewById(R.id.ef_switch_single)).isChecked();
        //final boolean useCustomImageLoader = ((Switch) findViewById(R.id.ef_switch_imageloader)).isChecked();
        //final boolean folderMode = ((Switch) findViewById(R.id.ef_switch_folder_mode)).isChecked();
        ImagePicker imagePicker = ImagePicker.create(this)
                .theme(R.style.ImagePickerTheme)
                .returnAfterFirst(false) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                .folderMode(false) // set folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select"); // image selection title

        //if (useCustomImageLoader) {
        //    attachButton.imageLoader(new GrayscaleImageLoader());
        //}

        /*
         (isSingleMode) {
            attachButton.single();
         else {
            attachButton.multi(); // multi mode (default mode)
        }
        */

        imagePicker.limit(10) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
                .imageFullDirectory(Environment.getExternalStorageDirectory().getPath()) // can be full path
                .origin(images) // original selected images, used in multi mode
                .start(RC_CODE_PICKER); // start image picker activity with request code
    }
    private void setStatus(){
        if(user.online){
            friend_status.setText("Online");
        } else {
            friend_status.setText("Offline");
        }
    }

    private void markAsRead(){
        VKRequest markAsRead = new VKRequest("messages.markAsRead", VKParameters.from("peer_id", user.id));
        markAsRead.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                //Toast.makeText(DialogActivity.this, "Marked as read", Toast.LENGTH_SHORT).show();
                 /*
                VKList<VKApiDialog> list = ((MyApp) getApplicationContext()).getDialogsList();
                for(VKApiDialog a : list){
                    if(a.message.user_id == messages.get(messages.size() - 1).user_id && a.message.isChat == isChat){
                        a.message = messages.get(messages.size() - 1);
                        a.unread = 0;
                        VKList<VKApiDialog> temp = new VKList<VKApiDialog>();
                        temp.add(a);
                        msgs_db.addOrUpdateDialogs(temp);
                    }
                }
*/
            }
        });
    }


    private BroadcastReceiver mUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (LongPollBroadcastService.ACTION_MESSAGE_ADDED.equals(action)) {
                Log.d("LONGPOLL", "MESSAGE ADDED IN DIALOGACT WITH FLAGS " + intent.getIntExtra("flags", 0));
                int user_id = intent.getIntExtra("user_id", 0);
                int flags = intent.getIntExtra("flags", 0);
                long time = intent.getLongExtra("time", 0);


                if(user_id == user.id && isInitialized){
                    setStatus();
                    final VKApiMessage extra_msg = new VKApiMessage();

                    updateMain = true;


                    extra_msg.id = intent.getIntExtra("id", 0);
                    extra_msg.body = intent.getStringExtra("body");
                    extra_msg.user_id = user_id;
                    extra_msg.date = time;

                    //loading attachments
                    //Log.d("LONG", "512 & 0x200 == " + (512 & 0x200));
                    if((flags & 0b1000000000) == 512){
                        Log.d("LONGPOLL", "ATTACHMENT_FLAG");
                        VKRequest req = new VKRequest("messages.getById", VKParameters.from("message_ids", extra_msg.id));
                        req.executeWithListener(new VKRequest.VKRequestListener() {
                            @Override
                            public void onComplete(VKResponse response) {
                                super.onComplete(response);
                                try {
                                    VKApiMessage msg = new VKApiMessage(response.json.optJSONObject("response").optJSONArray("items").optJSONObject(0));
                                    //messages.getById(msg.id).attachments = msg.attachments;
                                    //extra_msg.attachments = msg.attachments;
                                    for(int i = messages.size() - 1; i >= 0; i--){
                                        if(messages.get(i).id == msg.id){
                                            messages.get(i).attachments = msg.attachments;
                                            messagesAdapter.notifyItemChanged(i);
                                            break;
                                        }
                                    }

                                    Log.d("LONGPOLL", "EXTRA LONG POLL ATTACHMENT LOADED");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.d("LONGPOLL", "messages.getById in DialogActivity ERROR");
                                }
                            }
                        });
                    } else {
                        Log.d("LONGPOLL", "ATTACHMENT_FLAG IS NOT EXIST");
                    }

                    //inbox bit flag
                    if((flags & 0x0002) == 0){
                        extra_msg.out = false;
                    } else {
                        extra_msg.out = true;
                    }

                    if((flags & 0x0001) == 1){
                        extra_msg.read_state = false;
                    } else {
                        extra_msg.read_state = true;
                    }





                    if(messages.getById(extra_msg.id) == null){
                        //dialogRecyclerView.setHasFixedSize(true);
                        messages.add(extra_msg);
                        messagesAdapter.notifyItemInserted(messagesAdapter.getItemCount() - 1);
                        dialogRecyclerView.smoothScrollToPosition(messagesAdapter.getItemCount() - 1);
                    } else {

                        messages.getById(extra_msg.id).date = extra_msg.date;
                        messagesAdapter.notifyDataSetChanged();
                    }



                }

                if(!isInBackground)
                    markAsRead();

            }

            if(LongPollBroadcastService.ACTION_ALL_OUTBOX_MESSAGES_READ.equals(action)){
                int peer_id = intent.getIntExtra("peer_id", 0);
                int local_id = intent.getIntExtra("local_id", 0);
                if(peer_id == user.id){
                    for(VKApiMessage m : messages){
                        if(m.id <= local_id && m.out)
                            m.read_state = true;
                    }
                    messagesAdapter.notifyDataSetChanged();
                }
            }

            if(LongPollBroadcastService.ACTION_USER_TYPING_DIALOG.equals(action)){
                int user_id = intent.getIntExtra("id", 0);
                int flag = intent.getIntExtra("flag", 0);
                if(user_id == user.id){
                    friend_status.setText(getApplicationContext().getString(R.string.typing));
                    if(typingTimer != null){
                        typingTimer.cancel();
                        typingTimer.start();
                    }
                }

            }

            if(LongPollBroadcastService.ACTION_ALL_INBOX_MESSAGES_READ.equals(action)){
                int peer_id = intent.getIntExtra("peer_id", 0);
                int local_id = intent.getIntExtra("local_id", 0);
                if(peer_id == user.id){
                    for(VKApiMessage m : messages){
                        if(m.id <= local_id && !m.out)
                            m.read_state = true;
                    }
                    messagesAdapter.notifyDataSetChanged();
                }
            }

        }
    };


    CountDownTimer typingTimer = new CountDownTimer(5500, 1000){

        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            setStatus();
        }
    };

    private void setMainRecyclerView() {
        messagesAdapter = new RVMessagesAdapter(this, messages, messages_queue);
        dialogRecyclerView.setHasFixedSize(true);
        //dialogRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        dialogRecyclerView.setAdapter(messagesAdapter);
    }

    @Override
    public void onBackPressed() {

        VKList<VKApiDialog> list = ((MyApp) getApplicationContext()).getDialogsList();
        for(VKApiDialog a : list){
            if(messages.size() != 0 && a.message.user_id == messages.get(messages.size() - 1).user_id && a.message.isChat == isChat){
                a.message = messages.get(messages.size() - 1);

            }
        }

        Intent intent = new Intent();
        intent.putExtra(UPDATE_FLAG, updateMain);
        setResult(RESULT_OK, intent);

        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, Intent data) {
        if (requestCode == RC_CODE_PICKER && resultCode == RESULT_OK && data != null) {

            images = (ArrayList<Image>) ImagePicker.getImages(data);
            ((AttachmentsPanelAdapter) mAttachmentsAdapter).updateImagesSet(images);
            for (int i = 0; i < images.size(); i++) {
                boolean addFlag = true;
                for (int j = 0; j < mAttachmentsDataSet.size(); j++) {
                    //Checking if this photo already selected, or we added new one
                    //TODO WORKS ONLY FOR PHOTOS
                    if(((AttachmentCardPhoto) mAttachmentsDataSet.get(j)).getPath().equals(images.get(i).getPath())){

                        addFlag = false;
                        break;
                    }
                }
                //adding the photo to DataSet and making upload requests queue (positions of the attachments will be kept)
                if(addFlag){
                    final AttachmentCardPhoto photo = new AttachmentCardPhoto(images.get(i).getPath());
                    photo.setUploading(true);
                    mAttachmentsDataSet.add(photo);
                    mAttachmentsAdapter.notifyItemInserted(i);

                    VKRequest.VKRequestListener listener = new VKRequest.VKRequestListener() {
                        @Override
                        public void onComplete(VKResponse response) {

                            super.onComplete(response);
                            Log.d("UPLOAD", response.responseString);


                            JSONObject resp = response.json.optJSONArray("response").optJSONObject(0);

                            photo.setOwnerId(resp.optString("owner_id"));
                            photo.setItemId(resp.optString("id"));
                            photo.setUploading(false);
                            mAttachmentsAdapter.notifyDataSetChanged();


                        }
                    };

                    photo.startUpload(listener);

                    /*
                    File uploadImg = new File(images.get(i).getPath());
                    final VKRequest uploadImgRequest = new VKUploadMessagesPhotoRequest(uploadImg);
                    //creating ArrayList of Requests to make it possible to cancel loading of photo by index
                    photoUploadRequests.add(uploadImgRequest);
                    final int finalI = i;
                    uploadImgRequest.executeWithListener(new VKRequest.VKRequestListener() {
                            @Override
                            public void onComplete(VKResponse response) {

                                super.onComplete(response);
                                Log.d("UPLOAD", response.responseString);
                                photo.setUploading(false);
                                mAttachmentsAdapter.notifyDataSetChanged();
                                photoUploadRequests.remove(uploadImgRequest);
                                JSONObject resp = response.json.optJSONArray("response").optJSONObject(0);
                                //adding the photo URIs with right order (Requests are asynchronous, that's why it's important)
                                //photoAttachments.add(finalI, "photo" + resp.optString("owner_id") + "_" + resp.optString("id"));
                                photoAttachments[finalI] = ("photo" + resp.optString("owner_id") + "_" + resp.optString("id"));

                            }
                        });
                    */
                }

            }
            return;
        }

        if (requestCode == RC_CAMERA && resultCode == RESULT_OK) {
            /*
            getCameraModule().getImage(this, data, new OnImageReadyListener() {
                @Override
                public void onImageReady(List<Image> resultImages) {
                    images = (ArrayList<Image>) resultImages;
                    printImages(images);
                }
            });
            */
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private class AsyncMessagesAdd extends AsyncTask<VKList<VKApiMessage>, Void, VKList<VKApiMessage>> {

        @SafeVarargs
        @Override
        protected final VKList<VKApiMessage> doInBackground(VKList<VKApiMessage>... params) {

            //msgDbHelper.addMessages(params[0]);

            //messagesAdapter.updateAdapter(params[0]);
            return params[0];
        }

        @Override
        protected void onPostExecute(VKList<VKApiMessage> messages) {
            super.onPostExecute(messages);
            //messagesAdapter.updateAdapter(messages);
        }
    }



}
