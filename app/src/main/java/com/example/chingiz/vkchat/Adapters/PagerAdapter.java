package com.example.chingiz.vkchat.Adapters;

/**
 * Created by Admin on 12.09.2015.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private final int mNumOfTabs;
    private final Fragment DialogsFragment;
    private final Fragment OnlineFriendsFragment;
    private final Fragment AllFriendsFragment;

    public PagerAdapter(FragmentManager fm, int NumOfTabs, Fragment DialogsFragment, Fragment OnlineFriendsFragment, Fragment AllFriendsFragment) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.DialogsFragment = DialogsFragment;
        this.OnlineFriendsFragment = OnlineFriendsFragment;
        this.AllFriendsFragment = AllFriendsFragment;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return DialogsFragment;

            case 1:
                return OnlineFriendsFragment;

            case 2:
                return AllFriendsFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}