package com.example.chingiz.vkchat.ExtendedAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class VKApiAudioMsg {

    public int duration;
    int [] waveform;
    public String link_ogg;
    public String link_mp3;

    public VKApiAudioMsg(JSONObject jo) throws JSONException {
        if(jo != null)
            parse(jo);
    }

    public VKApiAudioMsg parse (JSONObject jo) throws JSONException {
        JSONArray wave = jo.optJSONArray("waveform");
        waveform = new int[wave.length()];
        for(int i = 0; i < wave.length(); i++){
            waveform[i] = wave.getInt(i);
        }

        duration = jo.optInt("duration");
        link_ogg = jo.optString("link_ogg");
        link_mp3 = jo.optString("link_mp3");
        return this;
    }
}
