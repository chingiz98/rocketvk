package com.example.chingiz.vkchat.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.vk.sdk.api.model.VKApiCity;
import com.vk.sdk.api.model.VKApiCountry;
import com.vk.sdk.api.model.VKApiDialog;
import com.vk.sdk.api.model.VKApiMessage;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;
import com.vk.sdk.api.model.VKUsersArray;

/**
 * Created by chingiz on 11.04.2016.
 */
public class CacheDB {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "DialogsCache.db";
    private static final String TEXT = " TEXT,";
    private static final String INTEGER = " INTEGER,";

    private static final String SQL_CREATE_ENTRIES_DIALOGS =
            "CREATE TABLE " + DialogsCacheContract.DialogsCacheEntry.TABLE_NAME + " (" +
                    DialogsCacheContract.DialogsCacheEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    DialogsCacheContract.DialogsCacheEntry.ID + TEXT +
                    DialogsCacheContract.DialogsCacheEntry.USER_ID + INTEGER +
                    DialogsCacheContract.DialogsCacheEntry.FROM_ID + INTEGER +
                    DialogsCacheContract.DialogsCacheEntry.BODY + TEXT +
                    DialogsCacheContract.DialogsCacheEntry.OUT + INTEGER +
                    DialogsCacheContract.DialogsCacheEntry.READ_STATE + INTEGER +
                    DialogsCacheContract.DialogsCacheEntry.UNREAD + INTEGER +
                    DialogsCacheContract.DialogsCacheEntry.TITLE + TEXT +
                    DialogsCacheContract.DialogsCacheEntry.DATE + INTEGER +
                    DialogsCacheContract.DialogsCacheEntry.CHAT_ID + INTEGER +
                    DialogsCacheContract.DialogsCacheEntry.CHAT_PHOTO_100 + " INTEGER" +
// Any other options for the CREATE command
                    " );";

    private static final String SQL_CREATE_ENTRIES_MESSAGES =
            "CREATE TABLE " + MessagesCacheContract.MessagesCacheEntry.TABLE_NAME + " (" +
                    MessagesCacheContract.MessagesCacheEntry._ID + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.TITLE + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.BODY + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.MSG_ID + TEXT +
                    //MessagesCacheContract.MessagesCacheEntry.FROM_ID + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.USER_ID + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.DATE + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.FIRST_NAME + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.LAST_NAME + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.READ_STATE + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.OUT + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.GEO + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.ATTACHMENTS + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.FWD_MESSAGES + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.EMOJI + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.IMPORTANT + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.DELETED + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.CHAT_ID + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.CHAT_ACTIVE + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PUSH_SETTINGS + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.USERS_COUNT + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.ADMIN_ID + INTEGER +
                    MessagesCacheContract.MessagesCacheEntry.ACTION + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.ACTION_EMAIL + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.ACTION_TEXT + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PHOTO_50_CHAT + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PHOTO_50_USER + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PHOTO_100_CHAT + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PHOTO_100_USER + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PHOTO_CHAT + TEXT +
                    MessagesCacheContract.MessagesCacheEntry.PHOTO_200_USER + " TEXT" +
// Any other options for the CREATE command
                    " );";

    private static final String SQL_CREATE_ENTRIES_USERS =
            "CREATE TABLE " + UsersCacheContract.FriendsCacheEntry.TABLE_NAME + " (" +
                    UsersCacheContract.FriendsCacheEntry._ID + INTEGER +
                    UsersCacheContract.FriendsCacheEntry.ID + TEXT +
                    UsersCacheContract.FriendsCacheEntry.FRIEND + INTEGER +
                    UsersCacheContract.FriendsCacheEntry.FIRST_NAME + TEXT +
                    UsersCacheContract.FriendsCacheEntry.LAST_NAME + TEXT +
                    UsersCacheContract.FriendsCacheEntry.BDATE + INTEGER +
                    UsersCacheContract.FriendsCacheEntry.ONLINE + INTEGER +
                    UsersCacheContract.FriendsCacheEntry.PHOTO_100 + TEXT +
                    UsersCacheContract.FriendsCacheEntry.PHOTO_MAX_ORIG + TEXT +
                    UsersCacheContract.FriendsCacheEntry.ORDER + TEXT +
                    UsersCacheContract.FriendsCacheEntry.HINTS + TEXT +
                    UsersCacheContract.FriendsCacheEntry.LAST_SEEN + INTEGER +
                    UsersCacheContract.FriendsCacheEntry.STATUS + TEXT +
                    UsersCacheContract.FriendsCacheEntry.SEX + TEXT +
                    UsersCacheContract.FriendsCacheEntry.CITY + TEXT +
                    UsersCacheContract.FriendsCacheEntry.COUNTRY + TEXT +
                    UsersCacheContract.FriendsCacheEntry.RELATION + INTEGER +
                    UsersCacheContract.FriendsCacheEntry.MOBILE_PHONE + TEXT +
                    UsersCacheContract.FriendsCacheEntry.HOME_PHONE + " TEXT" +
// Any other options for the CREATE command
                    " );";


    private static final String[] projection_dialogs = {
            DialogsCacheContract.DialogsCacheEntry._ID,
            DialogsCacheContract.DialogsCacheEntry.ID,
            DialogsCacheContract.DialogsCacheEntry.USER_ID,
            DialogsCacheContract.DialogsCacheEntry.FROM_ID,
            DialogsCacheContract.DialogsCacheEntry.BODY,
            DialogsCacheContract.DialogsCacheEntry.OUT,
            DialogsCacheContract.DialogsCacheEntry.READ_STATE,
            DialogsCacheContract.DialogsCacheEntry.UNREAD,
            DialogsCacheContract.DialogsCacheEntry.TITLE,
            DialogsCacheContract.DialogsCacheEntry.DATE,
            DialogsCacheContract.DialogsCacheEntry.CHAT_ID,
            DialogsCacheContract.DialogsCacheEntry.CHAT_PHOTO_100

    };

    private static final String[] projection_messages = {
            MessagesCacheContract.MessagesCacheEntry._ID,
            MessagesCacheContract.MessagesCacheEntry.FIRST_NAME,
            MessagesCacheContract.MessagesCacheEntry.LAST_NAME,
            MessagesCacheContract.MessagesCacheEntry.MSG_ID,
            MessagesCacheContract.MessagesCacheEntry.USER_ID,
            MessagesCacheContract.MessagesCacheEntry.DATE,
            MessagesCacheContract.MessagesCacheEntry.READ_STATE,
            MessagesCacheContract.MessagesCacheEntry.OUT,
            MessagesCacheContract.MessagesCacheEntry.TITLE,
            MessagesCacheContract.MessagesCacheEntry.BODY,
            MessagesCacheContract.MessagesCacheEntry.GEO,
            MessagesCacheContract.MessagesCacheEntry.ATTACHMENTS,
            MessagesCacheContract.MessagesCacheEntry.FWD_MESSAGES,
            MessagesCacheContract.MessagesCacheEntry.EMOJI,
            MessagesCacheContract.MessagesCacheEntry.IMPORTANT,
            MessagesCacheContract.MessagesCacheEntry.DELETED,
            MessagesCacheContract.MessagesCacheEntry.CHAT_ID,
            MessagesCacheContract.MessagesCacheEntry.CHAT_ACTIVE,
            MessagesCacheContract.MessagesCacheEntry.PUSH_SETTINGS,
            MessagesCacheContract.MessagesCacheEntry.USERS_COUNT,
            MessagesCacheContract.MessagesCacheEntry.ADMIN_ID,
            MessagesCacheContract.MessagesCacheEntry.ACTION,
            MessagesCacheContract.MessagesCacheEntry.ACTION_EMAIL,
            MessagesCacheContract.MessagesCacheEntry.ACTION_TEXT,
            MessagesCacheContract.MessagesCacheEntry.PHOTO_50_CHAT,
            MessagesCacheContract.MessagesCacheEntry.PHOTO_50_USER,
            MessagesCacheContract.MessagesCacheEntry.PHOTO_100_CHAT,
            MessagesCacheContract.MessagesCacheEntry.PHOTO_100_USER,
            MessagesCacheContract.MessagesCacheEntry.PHOTO_CHAT,
            MessagesCacheContract.MessagesCacheEntry.PHOTO_200_USER

    };

    private static final String[] projection_users = {
            UsersCacheContract.FriendsCacheEntry._ID,
            UsersCacheContract.FriendsCacheEntry.ID,
            UsersCacheContract.FriendsCacheEntry.FRIEND,
            UsersCacheContract.FriendsCacheEntry.FIRST_NAME,
            UsersCacheContract.FriendsCacheEntry.LAST_NAME,
            UsersCacheContract.FriendsCacheEntry.BDATE,
            UsersCacheContract.FriendsCacheEntry.ONLINE,
            UsersCacheContract.FriendsCacheEntry.PHOTO_100,
            UsersCacheContract.FriendsCacheEntry.PHOTO_MAX_ORIG,
            UsersCacheContract.FriendsCacheEntry.ORDER,
            UsersCacheContract.FriendsCacheEntry.HINTS,
            UsersCacheContract.FriendsCacheEntry.LAST_SEEN,
            UsersCacheContract.FriendsCacheEntry.STATUS,
            UsersCacheContract.FriendsCacheEntry.SEX,
            UsersCacheContract.FriendsCacheEntry.CITY,
            UsersCacheContract.FriendsCacheEntry.COUNTRY,
            UsersCacheContract.FriendsCacheEntry.RELATION

    };

    private static final String SQL_DELETE_ENTRIES_MESSAGES =
            "DROP TABLE IF EXISTS " + MessagesCacheContract.MessagesCacheEntry.TABLE_NAME;
    private static final String SQL_DELETE_ENTRIES_USERS =
            "DROP TABLE IF EXISTS " + MessagesCacheContract.MessagesCacheEntry.TABLE_NAME;


    private final CacheDBHelper DBHelper;
    private SQLiteDatabase db;

    public CacheDB(Context context){
        DBHelper = new CacheDBHelper(context);
    }


    private class CacheDBHelper extends SQLiteOpenHelper {

        public CacheDBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }


            @Override
            public void onCreate (SQLiteDatabase db){
                db.execSQL(SQL_CREATE_ENTRIES_DIALOGS);
                db.execSQL(SQL_CREATE_ENTRIES_MESSAGES);
                db.execSQL(SQL_CREATE_ENTRIES_USERS);

        }

            @Override
            public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion){
                db.execSQL(SQL_DELETE_ENTRIES_MESSAGES);
                db.execSQL(SQL_DELETE_ENTRIES_USERS);
                onCreate(db);
        }

    }

    private void open() throws SQLiteException {
        db = DBHelper.getWritableDatabase();
    }

    //closes the database
    private void close(){
        DBHelper.close();
    }

    public void addOrUpdateDialogs(VKList<VKApiDialog> dialogs) {
        open();
        ContentValues values = new ContentValues();


        //TODO!!! Раздельное обновление диалогов и бесед. Иначе вылазит баг с перезаписыванием данных.
        for (int i = 0; i < dialogs.getCount(); i++) {
/*
            boolean updateFlag = false;
            Cursor cur;
            if(dialogs.get(i).message.isChat){
                cur = db.query(true, DialogsCacheContract.MessagesCacheEntry.TABLE_NAME, projection_dialogs,
                        DialogsCacheContract.MessagesCacheEntry.CHAT_ID + "=?",
                        new String[]{String.valueOf(dialogs.get(i).message.chat_id)},
                        null, null, null, null);
            } else {
                cur = db.query(true, DialogsCacheContract.MessagesCacheEntry.TABLE_NAME, projection_dialogs,
                        DialogsCacheContract.MessagesCacheEntry.ID + "=? AND " + DialogsCacheContract.MessagesCacheEntry.CHAT_ID + "=?",
                        new String[]{String.valueOf(dialogs.get(i).message.user_id), String.valueOf(dialogs.get(i).message.chat_id)},
                        null, null, null, null);
            }

            if(cur.moveToNext()) updateFlag = true;
*/


            values.clear();
            values.put(DialogsCacheContract.DialogsCacheEntry.ID, dialogs.get(i).message.id);
            values.put(DialogsCacheContract.DialogsCacheEntry.USER_ID, dialogs.get(i).message.user_id);
            //values.put(DialogsCacheContract.MessagesCacheEntry.FROM_ID, dialogs.get(i).);
            values.put(DialogsCacheContract.DialogsCacheEntry.BODY, dialogs.get(i).message.body);
            values.put(DialogsCacheContract.DialogsCacheEntry.OUT, dialogs.get(i).message.out);
            values.put(DialogsCacheContract.DialogsCacheEntry.READ_STATE, dialogs.get(i).message.read_state);
            values.put(DialogsCacheContract.DialogsCacheEntry.TITLE, dialogs.get(i).message.title);
            values.put(DialogsCacheContract.DialogsCacheEntry.DATE, dialogs.get(i).message.date);
            values.put(DialogsCacheContract.DialogsCacheEntry.CHAT_ID, dialogs.get(i).message.chat_id);
            values.put(DialogsCacheContract.DialogsCacheEntry.CHAT_PHOTO_100, dialogs.get(i).message.photo_100_chat);
            values.put(DialogsCacheContract.DialogsCacheEntry.UNREAD, dialogs.get(i).unread);

            String whereClause;
            String[] whereArgs;


            if(dialogs.get(i).message.isChat){
                whereClause = DialogsCacheContract.DialogsCacheEntry.CHAT_ID + " = ?";
                whereArgs = new String[] {
                        Integer.toString(dialogs.get(i).message.chat_id)
                };

            } else {
                whereClause = DialogsCacheContract.DialogsCacheEntry.USER_ID + " = ? AND " + DialogsCacheContract.DialogsCacheEntry.CHAT_ID + " = ?";
                whereArgs = new String[] {Integer.toString(dialogs.get(i).message.user_id), "0"};
            }

            Cursor cursor = db.query(true, DialogsCacheContract.DialogsCacheEntry.TABLE_NAME, projection_dialogs, whereClause, whereArgs, null, null, null, null);


            if(cursor.getCount() <= 0){
                db.insert(DialogsCacheContract.DialogsCacheEntry.TABLE_NAME, null, values);
                Log.d("DIALOGS_CACHE", "ADDED");
            } else {
                db.update(DialogsCacheContract.DialogsCacheEntry.TABLE_NAME, values, whereClause, whereArgs);
                Log.d("DIALOGS_CACHE", "UPDATED");
            }

            cursor.close();

        }

        //db.update(DialogsCacheContract.MessagesCacheEntry.TABLE_NAME, values, DialogsCacheContract.MessagesCacheEntry.CHAT_ID + " = " + json_msg.getString(JsonConstants.CHAT_ID), null);v

        close();

    }

    public VKList<VKApiDialog> getDialogs(){
        open();

        VKList<VKApiDialog> dialogs = new VKList<>();

        Cursor cur = db.query(true, DialogsCacheContract.DialogsCacheEntry.TABLE_NAME, projection_dialogs,
                null,
                null,
                null, null, DialogsCacheContract.DialogsCacheEntry.ID + " DESC", null);

        while (cur.moveToNext()) {
            VKApiDialog dialog = new VKApiDialog();
            dialog.message = new VKApiMessage();

            dialog.message.id = cur.getInt(cur.getColumnIndex(DialogsCacheContract.DialogsCacheEntry.ID));
            dialog.message.body = cur.getString(cur.getColumnIndex(DialogsCacheContract.DialogsCacheEntry.BODY));
            dialog.message.user_id = cur.getInt(cur.getColumnIndex(DialogsCacheContract.DialogsCacheEntry.USER_ID));
            dialog.message.out = cur.getInt(cur.getColumnIndex(DialogsCacheContract.DialogsCacheEntry.OUT)) == 1;
            dialog.message.read_state = cur.getInt(cur.getColumnIndex(DialogsCacheContract.DialogsCacheEntry.READ_STATE)) == 1;
            dialog.message.title = cur.getString(cur.getColumnIndex(DialogsCacheContract.DialogsCacheEntry.TITLE));
            dialog.message.date = cur.getInt(cur.getColumnIndex(DialogsCacheContract.DialogsCacheEntry.DATE));
            dialog.message.chat_id = cur.getInt(cur.getColumnIndex(DialogsCacheContract.DialogsCacheEntry.CHAT_ID));
            dialog.unread = cur.getInt(cur.getColumnIndex(DialogsCacheContract.DialogsCacheEntry.UNREAD));
            if(dialog.message.chat_id != 0) dialog.message.isChat = true;
            dialog.message.photo_100_chat = cur.getString(cur.getColumnIndex(DialogsCacheContract.DialogsCacheEntry.CHAT_PHOTO_100));

            dialogs.add(dialog);

        }

        cur.close();
        close();

        return dialogs;
    }

    public VKList<VKApiMessage> getMessages(int user_id) {
        open();

        VKList<VKApiMessage> messages = new VKList<>();

        Cursor cur = db.query(true, MessagesCacheContract.MessagesCacheEntry.TABLE_NAME, projection_messages,
                MessagesCacheContract.MessagesCacheEntry.USER_ID + "=?",
                new String[]{String.valueOf(user_id)},
                null, null, MessagesCacheContract.MessagesCacheEntry.MSG_ID + " ASC", null);

        while (cur.moveToNext()) {
            VKApiMessage msg = new VKApiMessage();
            msg.id = cur.getInt(cur.getColumnIndex(MessagesCacheContract.MessagesCacheEntry.MSG_ID));
            msg.body = cur.getString(cur.getColumnIndex(MessagesCacheContract.MessagesCacheEntry.BODY));
            msg.user_id = cur.getInt(cur.getColumnIndex(MessagesCacheContract.MessagesCacheEntry.USER_ID));
            msg.out = cur.getInt(cur.getColumnIndex(MessagesCacheContract.MessagesCacheEntry.OUT)) == 1;
            msg.read_state = cur.getInt(cur.getColumnIndex(MessagesCacheContract.MessagesCacheEntry.READ_STATE)) == 1;
            msg.title = cur.getString(cur.getColumnIndex(MessagesCacheContract.MessagesCacheEntry.TITLE));
            msg.date = cur.getInt(cur.getColumnIndex(MessagesCacheContract.MessagesCacheEntry.DATE));

            messages.add(msg);

        }

        cur.close();
        close();

        return messages;
    }

    public void addUsers(VKUsersArray users, boolean isFriends) {

        open();
        int friend_flag = 1;
        if(!isFriends) friend_flag = 0;
        String checkRecordQuery;

        for (int i = 0; i < users.getCount(); i++) {
            checkRecordQuery = "Select * from " + UsersCacheContract.FriendsCacheEntry.TABLE_NAME +
                    " where " +
                    UsersCacheContract.FriendsCacheEntry.ID + " = " + users.get(i).id;

            Cursor cursor = db.rawQuery(checkRecordQuery, null);

            if (cursor.getCount() <= 0) {
                //ADD
                ContentValues values = new ContentValues();
                values.clear();


                values.put(UsersCacheContract.FriendsCacheEntry.ID, users.get(i).id);
                values.put(UsersCacheContract.FriendsCacheEntry.FRIEND, friend_flag);
                values.put(UsersCacheContract.FriendsCacheEntry.FIRST_NAME, users.get(i).first_name);
                values.put(UsersCacheContract.FriendsCacheEntry.LAST_NAME, users.get(i).last_name);
                values.put(UsersCacheContract.FriendsCacheEntry.BDATE, users.get(i).bdate);
                values.put(UsersCacheContract.FriendsCacheEntry.ONLINE, users.get(i).online);
                values.put(UsersCacheContract.FriendsCacheEntry.PHOTO_100, users.get(i).photo_100);
                values.put(UsersCacheContract.FriendsCacheEntry.PHOTO_MAX_ORIG, users.get(i).photo_max_orig);
                values.put(UsersCacheContract.FriendsCacheEntry.LAST_SEEN, users.get(i).last_seen);
                values.put(UsersCacheContract.FriendsCacheEntry.STATUS, users.get(i).activity);
                //values.put(UsersCacheContract.FriendsCacheEntry.CITY, users.get(i).city.title);
                //values.put(UsersCacheContract.FriendsCacheEntry.SEX, users.get(i).sex);
                //values.put(UsersCacheContract.FriendsCacheEntry.COUNTRY, users.get(i).country.title);
                //values.put(UsersCacheContract.FriendsCacheEntry.RELATION, users.get(i).relation);

                db.insert(UsersCacheContract.FriendsCacheEntry.TABLE_NAME, null, values);

            } else {
                ContentValues values = new ContentValues();

                values.put(UsersCacheContract.FriendsCacheEntry.ID, users.get(i).id);
                values.put(UsersCacheContract.FriendsCacheEntry.FIRST_NAME, users.get(i).first_name);
                values.put(UsersCacheContract.FriendsCacheEntry.LAST_NAME, users.get(i).last_name);
                values.put(UsersCacheContract.FriendsCacheEntry.BDATE, users.get(i).bdate);
                values.put(UsersCacheContract.FriendsCacheEntry.ONLINE, users.get(i).online);
                values.put(UsersCacheContract.FriendsCacheEntry.PHOTO_100, users.get(i).photo_100);
                values.put(UsersCacheContract.FriendsCacheEntry.PHOTO_MAX_ORIG, users.get(i).photo_max_orig);
                values.put(UsersCacheContract.FriendsCacheEntry.LAST_SEEN, users.get(i).last_seen);
                values.put(UsersCacheContract.FriendsCacheEntry.STATUS, users.get(i).activity);
                //values.put(UsersCacheContract.FriendsCacheEntry.CITY, users.get(i).city.title);
                //values.put(UsersCacheContract.FriendsCacheEntry.SEX, users.get(i).sex);
                //values.put(UsersCacheContract.FriendsCacheEntry.COUNTRY, users.get(i).country.title);
                //values.put(UsersCacheContract.FriendsCacheEntry.RELATION, users.get(i).relation);


                db.update(UsersCacheContract.FriendsCacheEntry.TABLE_NAME, values, UsersCacheContract.FriendsCacheEntry.ID + " = " + users.get(i).id, null);
            }
            cursor.close();
        }


        close();
    }

    public VKUsersArray getFriends(){

        open();

        VKUsersArray users_from_db = new VKUsersArray();
        Cursor cur_fr = db.query(UsersCacheContract.FriendsCacheEntry.TABLE_NAME, projection_users, "friend =?", new String[]{"1"}, null, null, null);




        while (cur_fr.moveToNext()){
            VKApiUserFull current_user = new VKApiUserFull();
            current_user.first_name = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.FIRST_NAME));
            current_user.last_name = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.LAST_NAME));
            current_user.photo_100 = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.PHOTO_100));
            current_user.photo_max_orig = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.PHOTO_MAX_ORIG));
            current_user.bdate = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.BDATE));
            //current_user.online = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.MessagesCacheEntry.ONLINE));
            current_user.id = cur_fr.getInt(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.ID));

            current_user.last_seen = cur_fr.getLong(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.LAST_SEEN));
            current_user.activity = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.STATUS));
            current_user.sex = cur_fr.getInt(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.SEX));

            VKApiCity usr_city = (new VKApiCity());
            usr_city.title = (cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.COUNTRY)));
            current_user.city = usr_city;

            VKApiCountry usr_country = (new VKApiCountry());
            usr_country.title = (cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.COUNTRY)));
            current_user.country = usr_country;

            current_user.relation = cur_fr.getInt(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.RELATION));

            users_from_db.add(current_user);
        }

        cur_fr.close();
        close();

        return users_from_db;
    }

    public VKUsersArray getUsers(){
        open();

        VKUsersArray users_from_db = new VKUsersArray();
        Cursor cur_fr = db.query(UsersCacheContract.FriendsCacheEntry.TABLE_NAME, projection_users, "friend =?", new String[]{"0"}, null, null, null);




        while (cur_fr.moveToNext()){
            VKApiUserFull current_user = new VKApiUserFull();
            current_user.first_name = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.FIRST_NAME));
            current_user.last_name = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.LAST_NAME));
            current_user.photo_100 = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.PHOTO_100));
            current_user.photo_max_orig = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.PHOTO_MAX_ORIG));
            current_user.bdate = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.BDATE));
            //current_user.online = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.MessagesCacheEntry.ONLINE));
            current_user.id = cur_fr.getInt(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.ID));

            current_user.last_seen = cur_fr.getLong(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.LAST_SEEN));
            current_user.activity = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.STATUS));
            current_user.sex = cur_fr.getInt(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.SEX));

            VKApiCity usr_city = (new VKApiCity());
            usr_city.title = (cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.COUNTRY)));
            current_user.city = usr_city;

            VKApiCountry usr_country = (new VKApiCountry());
            usr_country.title = (cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.COUNTRY)));
            current_user.country = usr_country;

            current_user.relation = cur_fr.getInt(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.RELATION));

            users_from_db.add(current_user);
        }

        cur_fr.close();
        close();

        return users_from_db;
    }

// --Commented out by Inspection START (29.10.16, 18:46):
//    public VKApiMessage getLastMessage(int user_id) {
//        open();
//
//        Cursor cur = db.query(true, DialogsCacheContract.MessagesCacheEntry.TABLE_NAME, projection_dialogs,
//                DialogsCacheContract.MessagesCacheEntry.USER_ID + "=?",
//                new String[]{String.valueOf(user_id)},
//                null, null, DialogsCacheContract.MessagesCacheEntry.ID + " ASC", "1");
//
//        if (cur.getCount() > 0) {
//            cur.moveToFirst();
//
//            VKApiMessage msg = new VKApiMessage();
//            msg.id = cur.getInt(cur.getColumnIndex(DialogsCacheContract.MessagesCacheEntry.ID));
//            msg.body = cur.getString(cur.getColumnIndex(DialogsCacheContract.MessagesCacheEntry.BODY));
//            msg.user_id = cur.getInt(cur.getColumnIndex(DialogsCacheContract.MessagesCacheEntry.FROM_ID));
//            msg.out = cur.getInt(cur.getColumnIndex(DialogsCacheContract.MessagesCacheEntry.OUT)) == 1;
//            msg.read_state = cur.getInt(cur.getColumnIndex(DialogsCacheContract.MessagesCacheEntry.READ_STATE)) == 1;
//            msg.title = cur.getString(cur.getColumnIndex(DialogsCacheContract.MessagesCacheEntry.TITLE));
//            msg.date = cur.getInt(cur.getColumnIndex(DialogsCacheContract.MessagesCacheEntry.DATE));
//            close();
//            return msg;
//
//        } else
//
//            cur.close();
//            close();
//            return null;
//
//    }
// --Commented out by Inspection STOP (29.10.16, 18:46)

}
