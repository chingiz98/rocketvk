package com.example.chingiz.vkchat.LongPoll;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;


/**
 * Created by Admin on 22.09.2015.
 */
@SuppressWarnings("unused")
class LongPoll  {

    private boolean isCancelled = false;

    public LongPoll(LongPollServer server, Context mContext){
        String url = "http://" + server.server + "?act=a_check&key=" + server.key + "&ts=" + server.ts + "&wait=20&mode=2";
        LongPollRequest(url);



    }

    private void LongPollRequest(final String url){

        System.out.println("URL IS " + url);
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(url, new AsyncHttpResponseHandler() {


            @Override
            public void onStart() {
                // called before request is started
                System.out.println("LONG POLL STARTED");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String decoded = null;
                try {
                    decoded = new String(response, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                System.out.println("LONG POLL RESPONSE " + decoded);

                JSONObject jsonResponse = null;
                try {
                    jsonResponse = new JSONObject(decoded);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    System.out.println(jsonResponse.getString("ts"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }


}
