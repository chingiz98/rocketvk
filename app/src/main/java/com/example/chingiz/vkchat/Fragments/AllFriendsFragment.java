package com.example.chingiz.vkchat.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.chingiz.vkchat.MyApp;
import com.example.chingiz.vkchat.R;
import com.example.chingiz.vkchat.Adapters.RecyclerViewAdapterAllFriends;
import com.turingtechnologies.materialscrollbar.AlphabetIndicator;
import com.turingtechnologies.materialscrollbar.DragScrollBar;
import com.vk.sdk.api.model.VKUsersArray;

/**
 * Created by Admin on 12.09.2015.
 */
public class AllFriendsFragment extends Fragment {

    private RecyclerViewAdapterAllFriends adapter;


    public void notifyDataSetChanged(){
        adapter.notifyDataSetChanged();
    }

    public void notifyItemRangeChanged(){
        adapter.notifyItemRangeChanged(0, ((MyApp) getContext().getApplicationContext()).getFriends().size());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        VKUsersArray friends = ((MyApp) getContext().getApplicationContext()).getFriends();
        VKUsersArray online_friends = ((MyApp) getContext().getApplicationContext()).getOnlineFriends();

        System.out.println("ALL FRIENDS FRAGMENT CREATED");


        View v = inflater.inflate(R.layout.layout3, container, false);
        RecyclerView recycler = (RecyclerView) v.findViewById(R.id.recycler_friends);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));


//        System.out.println("FR CUR IS " + MainActivity.fr.getFriendsCursor().getCount());

        Log.d("MSG", "Array size is: " + friends.getCount());

        adapter = new RecyclerViewAdapterAllFriends(getActivity(), friends);
        recycler.setAdapter(adapter);

        DragScrollBar scrollBar = (DragScrollBar) v.findViewById(R.id.dragScrollBar);
        scrollBar.setIndicator(new AlphabetIndicator(getActivity()), true);
        //new TouchScrollBar(getActivity(), recycler, false).addIndicator(new AlphabetIndicator(getActivity()), true);

        return v;
    }
}
