package com.example.chingiz.vkchat.Adapters.AttachmentsCards;

import android.util.Log;

import com.vk.sdk.api.VKRequest;

/**
 * Created by Chingiz on 17.08.17.
 */

public abstract class AttachmentCard {
    private boolean uploading;
    protected String owner_id;
    protected String item_id;
    protected String URI = null;
    protected VKRequest uploadRequest;

    public void setItemId(String item_id){
        this.item_id = item_id;
    }

    public String getItemId(){
        return item_id;
    }

    public void setOwnerId(String owner_id){
        this.owner_id = owner_id;
    }

    public String getOwnerId(){
        return owner_id;
    }

    public void setUploading(boolean uploading){
        this.uploading = uploading;
    }

    public boolean isUploading(){
        return uploading;
    }

    public abstract String getURI();

    public abstract void startUpload(VKRequest.VKRequestListener listener);

    public void cancelUpload(){
        if(uploadRequest != null){
            uploadRequest.cancel();

            Log.d("UPLOAD", "PHOTO LOAD IS CANCELLED");
        }
    }
}
