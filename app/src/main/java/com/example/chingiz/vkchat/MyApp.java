package com.example.chingiz.vkchat;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.example.chingiz.vkchat.LongPoll.LongPollBroadcastService;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.ios.IosEmojiProvider;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.model.VKApiDialog;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;
import com.vk.sdk.api.model.VKUsersArray;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by chingiz on 06.09.2015.
 */
public class MyApp extends Application  {




    private VKUsersArray my_app_friends;


    private VKUsersArray friends;
    private VKUsersArray new_users;
    private VKUsersArray users;
    private VKList<VKApiDialog> dialogsList;
    private VKUsersArray online_friends;

    private long ts;
    private String key;
    private String server;

    private boolean isInitialized;
    private boolean isServiceRunning = false;



// --Commented out by Inspection START (29.10.16, 18:31):
//        VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
//            @Override
//            public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
//                if (newToken == null) {
//                    Toast.makeText(MyApp.this, "AccessToken invalidated", Toast.LENGTH_LONG).show();
//                    Intent intent = new Intent(MyApp.this, MainActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(intent);
//                }
//            }
//        };
// --Commented out by Inspection STOP (29.10.16, 18:31)

    public void clearDialogsList(){
        this.dialogsList.clear();
    }

    public void clearFriends(){
        this.friends.clear();
    }

    public void clearNewUsers(){
        this.new_users.clear();
    }

    public void clearOnlineFriends(){
        this.online_friends.clear();
    }


    public void addDialogs(VKList<VKApiDialog> dialogsList){
        this.dialogsList.addAll(dialogsList);
    }

    public void addFriends(VKUsersArray friends){
        this.friends.addAll(friends);
    }

    public void addNewUsers(VKUsersArray new_users_arr){
        this.new_users.addAll(new_users_arr);
    }

    public void addOnlineFriends(JSONArray online_friends) throws JSONException{
        for(int i = 0; i < online_friends.length(); i++){
            VKApiUserFull usr = new VKApiUserFull();
            usr.id = online_friends.getInt(i);
            this.online_friends.add(usr);
        }
    }

    public void updateFriendsListWithOnline(JSONArray online_friends) throws JSONException{
        for(int i = 0; i < online_friends.length(); i++){
            this.friends.getById(online_friends.getInt(i)).online = true;
        }
    }

    public void setFriends(VKUsersArray friends){
        this.friends = friends;
    }

    public void setNewUsers(VKUsersArray new_users_arr){
        this.new_users = new_users_arr;
    }

    public void setUsers(VKUsersArray users){
        this.users = users;
    }

    public void setDialogsList(VKList<VKApiDialog> dialogsList){
        this.dialogsList = dialogsList;
    }

    public void setOnlineFriends(JSONArray online_friends) throws JSONException {
        this.online_friends = new VKUsersArray();

        for(int i = 0; i < online_friends.length(); i++){
            VKApiUserFull usr = new VKApiUserFull();
            usr.id = online_friends.getInt(i);
            this.online_friends.add(usr);
        }

    }

    public void setOnlineFriends(VKUsersArray online_friends){
        this.online_friends = online_friends;
    }

    public VKUsersArray getFriends(){
        return friends;
    }

    public VKUsersArray getNewUsers(){
        return new_users;
    }

    public VKUsersArray getUsers(){
        return users;
    }

    public VKList<VKApiDialog> getDialogsList(){
        return dialogsList;
    }

    public VKUsersArray getOnlineFriends(){
        return online_friends;
    }

    private VKUsersArray getOnlineFriendsArray(JSONArray online_friends) throws JSONException {

        VKUsersArray online_friends_arr = new VKUsersArray();

        for(int i = 0; i < online_friends.length(); i++){
            VKApiUserFull usr = new VKApiUserFull();
            usr.id = online_friends.getInt(i);
            online_friends_arr.add(usr);
        }

        return online_friends_arr;
    }
        @Override
        public void onCreate() {
            super.onCreate();
            Log.d("ApplicationClass", "ApplicationClass onCreate called");
            //VKSdk.initialize(this);
            VKSdk.customInitialize(this,5060966,"5.50");
            friends = new VKUsersArray();
            new_users = new VKUsersArray();
            users = new VKUsersArray();
            dialogsList = new VKList<VKApiDialog>();
            online_friends = new VKUsersArray();

            EmojiManager.install(new IosEmojiProvider());

            if(!isServiceRunning){
                Log.d("ApplicationClass", "SERVICE IS RUNNING");
                startService(new Intent(getApplicationContext(), LongPollBroadcastService.class));
                isServiceRunning = true;
            }


        }


        public boolean isInitialized(){
            return isInitialized;
        }

        public void setInitialized(boolean init){
            isInitialized = init;
        }


}

