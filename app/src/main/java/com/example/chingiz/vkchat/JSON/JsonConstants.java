package com.example.chingiz.vkchat.JSON;

/**
 * Created by Admin on 04.10.2015.
 */
@SuppressWarnings("unused")
public class JsonConstants {
    public static final String MESSAGE = "message";
    public static final String RESPONSE = "response";
    public static final String ITEMS = "items";
    public static final String ID = "id";
    public static final String USER_ID = "user_id";
    public static final String DATE = "date";
    public static final String READ_STATE = "read_state";
    public static final String OUT = "out";
    public static final String TITLE = "title";
    public static final String BODY = "body";
    public static final String CHAT_ID = "chat_id";
    public static final String EMOJI = "emoji";
    public static final String PHOTO_100 = "photo_100";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";


}
