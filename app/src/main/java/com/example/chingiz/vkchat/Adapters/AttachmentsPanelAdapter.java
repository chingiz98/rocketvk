package com.example.chingiz.vkchat.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.esafirm.imagepicker.model.Image;
import com.example.chingiz.vkchat.Adapters.AttachmentsCards.AttachmentCard;
import com.example.chingiz.vkchat.Adapters.AttachmentsCards.AttachmentCardPhoto;
import com.example.chingiz.vkchat.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Chingiz on 17.08.17.
 */

public class AttachmentsPanelAdapter extends RecyclerView.Adapter<AttachmentsPanelAdapter.ViewHolder> {
    private final int TYPE_DEFAULT = 0;
    private final int TYPE_PHOTO = 1;
    private ArrayList<AttachmentCard> dataSet;
    private ArrayList<Image> image;
    private Context mContext;


    public AttachmentsPanelAdapter(Context context, ArrayList<AttachmentCard> dataSet, ArrayList<Image> image){
        this.dataSet = dataSet;
        this.image = image;
        mContext = context;
    }

    @Override
    public AttachmentsPanelAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder = null;
        View v =  LayoutInflater.from(parent.getContext()).inflate(R.layout.attachmet_panel, parent, false);
        if(viewType == TYPE_PHOTO){
            ImageView img = new ImageView(mContext);
            img.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            img.setMinimumHeight(200);
            img.setMinimumWidth(250);
            img.setId(R.id.attachment_panel_ph);

            ((CardView) ((RelativeLayout) v).findViewById(R.id.attachments_panel_cardview)).addView(img);

            holder = new PhotoViewHolder(v);
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(final AttachmentsPanelAdapter.ViewHolder holder, int position) {
        ProgressBar progressBar = holder.itemView.findViewById(R.id.attachmentProgressBar);
        if(dataSet.get(position).isUploading()){
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }

        View.OnClickListener close = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < image.size(); i++) {
                    if(image.get(i).getPath().equals(((AttachmentCardPhoto) dataSet.get(holder.getAdapterPosition())).getPath())){
                        image.remove(i);
                    }
                }
                dataSet.get(holder.getAdapterPosition()).cancelUpload();
                dataSet.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
            }
        };

        if(getItemViewType(position) == TYPE_PHOTO){
            PhotoViewHolder photoHolder = (PhotoViewHolder) holder;
            AttachmentCardPhoto photo = (AttachmentCardPhoto) dataSet.get(position);
            File photoFile = new File(photo.getPath());
            Picasso.with(mContext).load(photoFile).resize(250, 200).centerCrop().into(photoHolder.img);
            ((ImageView) ((PhotoViewHolder) holder).itemView.findViewById(R.id.close_attachment_button)).setOnClickListener(close);
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void updateImagesSet(ArrayList<Image> image){
        this.image = image;
    }


    @Override
    public int getItemViewType(int position) {
        if(dataSet.get(position).getClass() == AttachmentCardPhoto.class){
            return TYPE_PHOTO;
        }
        return TYPE_DEFAULT;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    class PhotoViewHolder extends ViewHolder{
        private ImageView img;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.attachment_panel_ph);


        }
    }
}
