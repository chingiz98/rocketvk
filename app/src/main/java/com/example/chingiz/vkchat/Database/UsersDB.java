package com.example.chingiz.vkchat.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKUsersArray;

/**
 * Created by chingiz on 09.10.2015.
 */
public class UsersDB {


    private static final String TEXT = " TEXT,";
    private static final String INTEGER = " INTEGER,";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + UsersCacheContract.FriendsCacheEntry.TABLE_NAME + " (" +
                    UsersCacheContract.FriendsCacheEntry._ID + INTEGER +
                    UsersCacheContract.FriendsCacheEntry.ID + TEXT +
                    UsersCacheContract.FriendsCacheEntry.FRIEND + INTEGER +
                    UsersCacheContract.FriendsCacheEntry.FIRST_NAME + TEXT +
                    UsersCacheContract.FriendsCacheEntry.LAST_NAME + TEXT +
                    UsersCacheContract.FriendsCacheEntry.BDATE + INTEGER +
                    UsersCacheContract.FriendsCacheEntry.ONLINE + INTEGER +
                    UsersCacheContract.FriendsCacheEntry.PHOTO_100 + TEXT +
                    UsersCacheContract.FriendsCacheEntry.ORDER + TEXT +
                    UsersCacheContract.FriendsCacheEntry.HINTS + " TEXT" +
// Any other options for the CREATE command
                    " );";


    private static final String[] projection = {
            UsersCacheContract.FriendsCacheEntry._ID,
            UsersCacheContract.FriendsCacheEntry.ID,
            UsersCacheContract.FriendsCacheEntry.FRIEND,
            UsersCacheContract.FriendsCacheEntry.FIRST_NAME,
            UsersCacheContract.FriendsCacheEntry.LAST_NAME,
            UsersCacheContract.FriendsCacheEntry.BDATE,
            UsersCacheContract.FriendsCacheEntry.ONLINE,
            UsersCacheContract.FriendsCacheEntry.PHOTO_100,
            UsersCacheContract.FriendsCacheEntry.ORDER,
            UsersCacheContract.FriendsCacheEntry.HINTS

    };

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + MessagesCacheContract.MessagesCacheEntry.TABLE_NAME;


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "FriendsCache.db";

    private final FriendsCacheDbHelper DBHelper;
    private SQLiteDatabase db;


    public UsersDB(Context ctx){
        DBHelper = new FriendsCacheDbHelper(ctx); //there would be an error initially but just keep going...
    }

    private static class FriendsCacheDbHelper extends SQLiteOpenHelper {


        public FriendsCacheDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }


        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }

    //opens the database
    private void open() throws SQLiteException {
        db = DBHelper.getWritableDatabase();
    }

    //closes the database
    private void close(){
        DBHelper.close();
    }

    public void addUsers(VKUsersArray users, boolean isFriends) {

        open();
            int friend_flag = 1;
            if(!isFriends) friend_flag = 0;
            String checkRecordQuery;

            for (int i = 0; i < users.getCount(); i++) {
                checkRecordQuery = "Select * from " + UsersCacheContract.FriendsCacheEntry.TABLE_NAME +
                        " where " +
                        UsersCacheContract.FriendsCacheEntry.ID + " = " + users.get(i).id;

                Cursor cursor = db.rawQuery(checkRecordQuery, null);

                if (cursor.getCount() <= 0) {
                    //ADD
                    ContentValues values = new ContentValues();
                    values.clear();


                    values.put(UsersCacheContract.FriendsCacheEntry.ID, users.get(i).id);
                    values.put(UsersCacheContract.FriendsCacheEntry.FRIEND, friend_flag);
                    values.put(UsersCacheContract.FriendsCacheEntry.FIRST_NAME, users.get(i).first_name);
                    values.put(UsersCacheContract.FriendsCacheEntry.LAST_NAME, users.get(i).last_name);
                    values.put(UsersCacheContract.FriendsCacheEntry.BDATE, users.get(i).bdate);
                    values.put(UsersCacheContract.FriendsCacheEntry.ONLINE, users.get(i).online);
                    values.put(UsersCacheContract.FriendsCacheEntry.PHOTO_100, users.get(i).photo_100);

                    db.insert(UsersCacheContract.FriendsCacheEntry.TABLE_NAME, null, values);

                } else {
                    ContentValues values = new ContentValues();

                    values.put(UsersCacheContract.FriendsCacheEntry.ID, users.get(i).id);
                    values.put(UsersCacheContract.FriendsCacheEntry.FIRST_NAME, users.get(i).first_name);
                    values.put(UsersCacheContract.FriendsCacheEntry.LAST_NAME, users.get(i).last_name);
                    values.put(UsersCacheContract.FriendsCacheEntry.BDATE, users.get(i).bdate);
                    values.put(UsersCacheContract.FriendsCacheEntry.ONLINE, users.get(i).online);
                    values.put(UsersCacheContract.FriendsCacheEntry.PHOTO_100, users.get(i).photo_100);

                    db.update(UsersCacheContract.FriendsCacheEntry.TABLE_NAME, values, UsersCacheContract.FriendsCacheEntry.ID + " = " + users.get(i).id, null);
                }
                cursor.close();
            }


        close();
    }

    public VKUsersArray getFriends(){

        open();

        VKUsersArray users_from_db = new VKUsersArray();
        Cursor cur_fr = db.query(UsersCacheContract.FriendsCacheEntry.TABLE_NAME, UsersDB.projection, "friend =?", new String[]{"1"}, null, null, null);




        while (cur_fr.moveToNext()){
            VKApiUserFull current_user = new VKApiUserFull();
            current_user.first_name = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.FIRST_NAME));
            current_user.last_name = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.LAST_NAME));
            current_user.photo_100 = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.PHOTO_100));
            current_user.bdate = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.BDATE));
            //current_user.online = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.MessagesCacheEntry.ONLINE));
            current_user.id = cur_fr.getInt(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.ID));

            users_from_db.add(current_user);
        }

        cur_fr.close();
        close();

        return users_from_db;
    }

// --Commented out by Inspection START (29.10.16, 18:39):
//    public VKApiUserFull getCurrentUserProfile(){
//        open();
//        VKApiUserFull current_user = new VKApiUserFull();
//
//        Cursor cur_current_user = db.query(UsersCacheContract.FriendsCacheEntry.TABLE_NAME, UsersDB.projection, UsersCacheContract.FriendsCacheEntry.ID,
//                new String[]{String.valueOf(VKAccessToken.currentToken().userId)}, null, null, null);
//        while (cur_current_user.moveToNext()){
//            current_user.first_name = cur_current_user.getString(cur_current_user.getColumnIndex(UsersCacheContract.FriendsCacheEntry.FIRST_NAME));
//            current_user.last_name = cur_current_user.getString(cur_current_user.getColumnIndex(UsersCacheContract.FriendsCacheEntry.LAST_NAME));
//            current_user.photo_100 = cur_current_user.getString(cur_current_user.getColumnIndex(UsersCacheContract.FriendsCacheEntry.PHOTO_100));
//            current_user.bdate = cur_current_user.getString(cur_current_user.getColumnIndex(UsersCacheContract.FriendsCacheEntry.BDATE));
//            //current_user.online = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.MessagesCacheEntry.ONLINE));
//            current_user.id = cur_current_user.getInt(cur_current_user.getColumnIndex(UsersCacheContract.FriendsCacheEntry.ID));
//        }
//
//        cur_current_user.close();
//        close();
//
//        return current_user;
//    }
// --Commented out by Inspection STOP (29.10.16, 18:39)

    public VKUsersArray getUsers(){
        open();

        VKUsersArray users_from_db = new VKUsersArray();
        Cursor cur_fr = db.query(UsersCacheContract.FriendsCacheEntry.TABLE_NAME, UsersDB.projection, "friend =?", new String[]{"0"}, null, null, null);




        while (cur_fr.moveToNext()){
            VKApiUserFull current_user = new VKApiUserFull();
            current_user.first_name = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.FIRST_NAME));
            current_user.last_name = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.LAST_NAME));
            current_user.photo_100 = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.PHOTO_100));
            current_user.bdate = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.BDATE));
            //current_user.online = cur_fr.getString(cur_fr.getColumnIndex(UsersCacheContract.MessagesCacheEntry.ONLINE));
            current_user.id = cur_fr.getInt(cur_fr.getColumnIndex(UsersCacheContract.FriendsCacheEntry.ID));

            users_from_db.add(current_user);
        }

        cur_fr.close();
        close();

        return users_from_db;
    }

}