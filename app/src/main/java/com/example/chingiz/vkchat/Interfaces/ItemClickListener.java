package com.example.chingiz.vkchat.Interfaces;

import android.view.View;

/**
 * Created by chingiz on 11.04.2016.
 */
public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}

