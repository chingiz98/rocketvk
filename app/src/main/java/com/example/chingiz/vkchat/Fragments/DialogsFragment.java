package com.example.chingiz.vkchat.Fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.chingiz.vkchat.Activities.MainActivity;
import com.example.chingiz.vkchat.Adapters.Footer;
import com.example.chingiz.vkchat.Database.CacheDB;
import com.example.chingiz.vkchat.MyApp;
import com.example.chingiz.vkchat.R;
import com.example.chingiz.vkchat.Adapters.RecyclerViewAdapterDialogs;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiCommunityArray;
import com.vk.sdk.api.model.VKApiDialog;
import com.vk.sdk.api.model.VKApiGetDialogResponse;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;
import com.vk.sdk.api.model.VKUsersArray;

import static com.vk.sdk.VKUIHelper.getApplicationContext;


/**
 * Created by Admin on 12.09.2015.
 */
public class DialogsFragment extends Fragment {



    public RecyclerView recycler;
    private RecyclerViewAdapterDialogs adapter;

    private VKList<VKApiDialog> dialogsList;
    private VKUsersArray users;
    private VKUsersArray new_users_arr;
    private AsyncTask asyncTask;

    private final int visibleThreshold = 5;

    private CacheDB cache_db;
    //private MyApp app = (MyApp) getContext().getApplicationContext();

    private SwipeRefreshLayout mSwipeRefreshDialogs;




    public void notifyDataSetChanged(){
        Log.d("MSG", "notifyDataSetChanged of fragment is called");
        try {
            Log.d("MSG", "Dialogs list size in DialogsFragment is: " + ((MyApp) getContext().getApplicationContext()).getDialogsList().size());
        } catch (Exception e){
            e.printStackTrace();
        }

        try {
            Log.d("MSG", "DIALOGS SIZE: " + ((MyApp) getContext().getApplicationContext()).getDialogsList().size());
            adapter.notifyDataSetChanged();
        } catch (Exception exp){
            Log.d("MSG", "EXCEPTION IS: " + exp.getMessage());

        }



        // save index and top position


        //Parcelable recyclerViewState;
        System.out.println("CHANGES ARE CURRENTLY IN DATABASE BUT NOT IN THE RECYCLERVIEW...");
        //final MessagesCacheDbHelper mDbHelper = new MessagesCacheDbHelper(getActivity());
        //final SQLiteDatabase db = mDbHelper.getWritableDatabase();
        //Cursor temp_cur = db.query(MessagesCacheContract.DialogsCacheEntry.TABLE_NAME, MessagesCacheDbHelper.projection, null, null, null, null, MessagesCacheContract.DialogsCacheEntry.DATE + " DESC");
        //MainActivity.dl.setDialogsCursor(temp_cur);
        //adapter.swapCursor(temp_cur);

       // recycler.getLayoutManager().onRestoreInstanceState(recyclerViewState);//restore

        //adapter.swapData(dialogsList);

        // ((TextView) getActivity().findViewById(R.id.textViewDMN)).setText("CHANGED");
    }


    public void setSwipeRefreshListener(SwipeRefreshLayout.OnRefreshListener lis){
        mSwipeRefreshDialogs.setOnRefreshListener(lis);
    }

    public void setSwipeRefreshLayoutRefreshing(boolean flag){
        mSwipeRefreshDialogs.setRefreshing(flag);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean hasFooter() {
        dialogsList = ((MyApp) getContext().getApplicationContext()).getDialogsList();
        return dialogsList.get(dialogsList.size() - 1) instanceof Footer;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("MSG", "onCreate of fragment is called");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("MSG", "onCreateView of fragment is called");
        new_users_arr = ((MyApp) getContext().getApplicationContext()).getNewUsers();
        users = ((MyApp) getContext().getApplicationContext()).getFriends();
        dialogsList = ((MyApp) getContext().getApplicationContext()).getDialogsList();
        View v = inflater.inflate(R.layout.layout1, container, false);
        recycler = (RecyclerView) v.findViewById(R.id.recycler_dialogs);
        // recycler.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        //mSwipeRefreshDialogs = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayoutDialogs);
        //mSwipeRefreshDialogs.setOnRefreshListener((MainActivity) getContext());

        //((MainActivity) getApplicationContext()).

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(linearLayoutManager);

        adapter = new RecyclerViewAdapterDialogs(getActivity(), dialogsList, users, new_users_arr);
        recycler.setAdapter(adapter);


        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int totalItemCount = linearLayoutManager.getItemCount();
                int lastVisibleItem = linearLayoutManager
                        .findLastVisibleItemPosition();
                super.onScrolled(recyclerView, dx, dy);
                if (totalItemCount <= (lastVisibleItem + visibleThreshold) && !(hasFooter())) {
                    Log.d("MSG", "SCROLL LISTENER");
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    //position starts at 0
                    if (layoutManager.findLastCompletelyVisibleItemPosition() == layoutManager.getItemCount() - 2) {
                        asyncTask = new LoadNewDialogs();
                        Void[] params = null;
                        asyncTask.execute(params);
                    }
                }
            }
        });



        Log.d("MSG", "onCreateView of fragment is ended");

        return v;
    }



    private class LoadNewDialogs extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            dialogsList = ((MyApp) getContext().getApplicationContext()).getDialogsList();
            dialogsList.add(new Footer());
            recycler.getAdapter().notifyItemInserted(dialogsList.size() - 1);
        }

        @Override
        protected Void doInBackground(Void... params) {
          try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                Log.e(this.getClass().toString(), e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            new_users_arr = ((MyApp) getContext().getApplicationContext()).getNewUsers();
            users = ((MyApp) getContext().getApplicationContext()).getFriends();
            dialogsList = ((MyApp) getContext().getApplicationContext()).getDialogsList();
            final int size = dialogsList.size();
            dialogsList.remove(size - 1);//removes footer

//            Log.d("MSG", "LAST MESSAGE ID IS " + dialogsList.get(dialogsList.size() - 1).message.id);
            int dialogs_count = 15;
            VKRequest request_dialogs = VKApi.messages().getDialogs(VKParameters.from(VKApiConst.COUNT, dialogs_count, VKApiConst.START_MESSAGE_ID, dialogsList.get(dialogsList.size() - 1).message.id - 1));

            request_dialogs.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {

                    String new_users = "";
                    boolean thereNewUsers = false;
                    boolean thereIsNewGroupsUsers = false;
                    String new_groups_users = ""; //for chats with communities


                    VKApiGetDialogResponse msg = (VKApiGetDialogResponse) response.parsedModel;
                    final VKList<VKApiDialog> resp = msg.items;
                    for(int i = 0; i < resp.getCount(); i++){
                        dialogsList.add(resp.get(i));
                        int current_user = Integer.parseInt(VKAccessToken.currentToken().userId); //Current AccessToken owner
                           if(users.getById(resp.get(i).message.user_id) == null
                                   && current_user != resp.get(i).message.user_id){ //if not in usersList and not current User

                               new_users = new_users + resp.get(i).message.user_id + ",";
                               thereNewUsers = true;
                           }

                        if(resp.get(i).message.user_id < 0){
                            new_groups_users = new_groups_users + Math.abs(resp.get(i).message.user_id) + ",";
                            thereIsNewGroupsUsers = true;
                        }

                    }

                    if(thereNewUsers){
                        VKRequest get_user = VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS, new_users, VKApiConst.FIELDS, MainActivity.USER_FIELDS, "order", "name"));
                        get_user.executeWithListener(new VKRequest.VKRequestListener() {
                            @Override
                            public void onComplete(VKResponse response) {
                                VKList<VKApiUserFull> users_resp = (VKList<VKApiUserFull>) response.parsedModel;
                                new_users_arr.addAll(users_resp);
                                recycler.getAdapter().notifyDataSetChanged();
                                //recycler.getAdapter().notifyItemRangeChanged(size - 1, dialogsList.size() - size);
                                //Adding users (not friends) to database
                                cache_db = new CacheDB(getContext());
                                cache_db.addUsers(new_users_arr, false);
                            }
                        });
                    } else {
                        //recycler.getAdapter().notifyItemRangeChanged(size - 1, dialogsList.size() - size);
                        recycler.getAdapter().notifyDataSetChanged();
                    }

                    if(thereIsNewGroupsUsers){ //Adding new communities dialogs

                        VKRequest get_groups = VKApi.groups().getById(VKParameters.from(VKApiConst.GROUP_IDS, new_groups_users));

                        get_groups.executeWithListener(new VKRequest.VKRequestListener() {

                            @Override
                            public void onComplete(VKResponse response) {
                                super.onComplete(response);
                                VKApiCommunityArray groupsArray = (VKApiCommunityArray) response.parsedModel;
                                VKUsersArray groupsToUsers = new VKUsersArray();

                                for(int i = 0; i < groupsArray.size(); i++){
                                    VKApiUserFull tmp = new VKApiUserFull();
                                    tmp.first_name = groupsArray.get(i).name;
                                    tmp.last_name = "";
                                    tmp.photo_50 = groupsArray.get(i).photo_50;
                                    tmp.photo_100 = groupsArray.get(i).photo_100;
                                    tmp.photo_200 = groupsArray.get(i).photo_200;
                                    tmp.id = -groupsArray.get(i).id;
                                    tmp.online = false;
                                    //TODO ADD screen_name field
                                    groupsToUsers.add(tmp);
                                }

                                ((MyApp) getApplicationContext()).getNewUsers().addAll(groupsToUsers);
                                cache_db = new CacheDB(getContext());
                                cache_db.addUsers(groupsToUsers, false);
                                recycler.getAdapter().notifyDataSetChanged();

                            }
                        });


                    }


                    //notifyDataSetChanged();
                }
            });


        }

    }


    public interface OnHeadlineSelectedListener {
    }


}
