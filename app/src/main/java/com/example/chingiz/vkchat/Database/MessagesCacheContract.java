package com.example.chingiz.vkchat.Database;

import android.provider.BaseColumns;

/**
 * Created by chingiz on 03.10.2015.
 */
class MessagesCacheContract {


    /* Inner class that defines the table contents */
        public static abstract class MessagesCacheEntry implements BaseColumns {

            public static final String TABLE_NAME = "messages_cache";
            public static final String _ID = "_id";

            public static final String MSG_ID = "msg_id";
            //public static final String FROM_ID = "from_id";
            public static final String USER_ID = "user_id";
            public static final String DATE = "date";
            public static final String FIRST_NAME = "first_name";
            public static final String LAST_NAME = "last_name";
            public static final String READ_STATE = "read_state";
            public static final String OUT = "out";
            public static final String TITLE = "title";
            public static final String BODY = "body";
            public static final String GEO = "geo";
            public static final String ATTACHMENTS = "attachments";
            public static final String FWD_MESSAGES = "fwd_messages";
            public static final String EMOJI = "emoji";
            public static final String IMPORTANT = "important";
            public static final String DELETED = "deleted";
            public static final String PHOTO_50_USER = "photo_50_user";
            public static final String PHOTO_100_USER = "photo_100_user";
            public static final String PHOTO_200_USER = "photo_200_user";
            //CHAT FIELDS
            public static final String CHAT_ID = "chat_id";
            public static final String CHAT_ACTIVE = "chat_active";
            public static final String PUSH_SETTINGS = "push_settings";
            public static final String USERS_COUNT = "users_count";
            public static final String ADMIN_ID = "admin_id";
            public static final String ACTION = "action";
            // --Commented out by Inspection (29.10.16, 18:46):public static final String ACTION_MID = "action_mid";
            public static final String ACTION_EMAIL = "action_email";
            public static final String ACTION_TEXT = "action_text";
            public static final String PHOTO_50_CHAT = "photo_50_chat";
            public static final String PHOTO_100_CHAT = "photo_100_chat";
            public static final String PHOTO_CHAT = "photo_chat";









        }


}
