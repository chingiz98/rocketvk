package com.example.chingiz.vkchat.Activities;

import android.os.Bundle;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.chingiz.vkchat.R;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiUser;

/**
 * Created by chingiz on 11.02.2017.
 */

public class ProfileActivity extends AppCompatActivity {

    private VKApiUser user;

    private ImageView mProfileImage;
    private CollapsingToolbarLayout mCollapsingToolbar;
    private AppBarLayout mAppBarLayout;
    private TextView mTestTextview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mAppBarLayout = (AppBarLayout) findViewById(R.id.appbar_profile_activity);

        user = getIntent().getParcelableExtra("user");

        mCollapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_profile_activity);
        mCollapsingToolbar.setTitle(user.first_name + " " + user.last_name);

        mProfileImage = (ImageView) findViewById(R.id.toolbar_image_profile_activity);
        Picasso.with(this).load(user.photo_max_orig).into(mProfileImage);

        mTestTextview = (TextView) findViewById(R.id.test_profile_text);

        AppBarLayout scrollView = (AppBarLayout)findViewById(R.id.appbar_profile_activity);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams)scrollView.getLayoutParams();
        params.setBehavior(new SmoothScrollBehavior(R.id.nested_profile_activity));

        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int scrollRange = appBarLayout.getTotalScrollRange();
                float percentage = (1.0f - ((float) Math.abs(verticalOffset)/scrollRange));
                mTestTextview.setAlpha(percentage);
            }
        });
    }

}

