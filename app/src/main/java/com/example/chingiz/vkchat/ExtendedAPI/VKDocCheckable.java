package com.example.chingiz.vkchat.ExtendedAPI;

import com.vk.sdk.api.model.VKApiDocument;

/**
 * Created by Chingiz on 11.02.2018.
 */

public class VKDocCheckable extends VKApiDocument {
    private boolean checked;

    public VKDocCheckable(){
        super();
        checked = false;
    }

    public void check(){
        checked = !checked;
    }

    public boolean getChecked(){
        return checked;
    }
}
