package com.example.chingiz.vkchat.LongPoll;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Chingiz on 05.02.17.
 */

public class LongPollBroadcastService extends Service {

    private boolean running = false;
    public final static String ACTION = "com.RocketVK.ACTION";
    public final static String ACTION_MESSAGE_ADDED = "com.RocketVK.ACTION_MESSAGE_ADDED";
    public final static String ACTION_MESSAGE_FLAGS_RESET = "com.RocketVK.ACTION_MESSAGE_FLAGS_RESET";
    public final static String ACTION_ALL_OUTBOX_MESSAGES_READ = "com.RocketVK.ACTION_ALL_OUTBOX_MESSAGES_READ";
    public final static String ACTION_ALL_INBOX_MESSAGES_READ = "com.RocketVK.ACTION_ALL_INBOX_MESSAGES_READ";
    public final static String ACTION_USER_TYPING_DIALOG = "com.RocketVK.ACTION_USER_TYPING_DIALOG";
    public final static String ACTION_USER_TYPING_CHAT = "com.RocketVK.ACTION_USER_TYPING_CHAT";

    private String key;
    private String server;
    private long ts;
    private static final int MAX_REQUEST_RETRIES = 5;

    private static final int MESSAGE_ADDED = 4;
    private static final int MESSAGE_FLAGS_RESET = 3;
    private static final int ALL_OUTBOX_MESSAGES_READ = 7;
    private static final int ALL_INBOX_MESSAGES_READ = 6;
    private static final int USER_TYPING_DIALOG = 61;
    private static final int USER_TYPING_CHAT= 62;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {



        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("MSG", "LONG_POLL SERVICE IS STARTED");
        running = true;

        //running = true;
        if(key == null || server == null || ts == 0){
            getLongPollServer();
        } else {
            longPoll();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.d("MSG", "TASK REMOVED");
    }

    //starts long poll request for the first time
    private void getLongPollServer(){
        String REQUEST = "messages.getLongPollServer";
        VKRequest req = new VKRequest(REQUEST);
        req.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                Log.d("Response", response.json.toString());
                try {
                    JSONObject resp = response.json.getJSONObject("response");
                    key = resp.getString("key");
                    server = resp.getString("server");
                    ts = resp.getLong("ts");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                longPoll();
            }
        });
    }

    //long poll request
    private void longPoll(){

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://" + server + "?act=a_check&key=" + key + "&ts=" + ts + "&wait=20&mode=2&version=1";

        //final String url = "http://httpbin.org/get?param1=hello";

        // prepare the Request
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        Log.d("Response", response.toString());
                        //final Intent intent = new Intent(ACTION);
                        //intent.putExtra();
                        //sendBroadcast(intent);

                            ts = response.optLong("ts");
                            //starting long poll request again
                            longPoll();

                            int failed = response.optInt("failed");

                            if(failed == 1 || failed == 2 || failed == 3){
                                getLongPollServer();
                                return;
                            }

                            JSONArray updates = response.optJSONArray("updates");
                            processUpdates(updates);



                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                        if(key == null || server == null || ts == 0){
                            getLongPollServer();
                        }
                    }
                }
        );

        getRequest.setRetryPolicy(new DefaultRetryPolicy(90000, MAX_REQUEST_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // add it to the RequestQueue
        queue.add(getRequest);


    }

    private void processUpdates(JSONArray updates){
        for(int i = 0; i < updates.length(); i++){
            int cur = updates.optJSONArray(i).optInt(0);
            Intent intent = new Intent();
            switch (cur){

                case MESSAGE_ADDED:
                    intent.setAction(ACTION_MESSAGE_ADDED);
                    intent.putExtra("id", updates.optJSONArray(i).optInt(1));
                    intent.putExtra("flags", updates.optJSONArray(i).optInt(2));
                    intent.putExtra("user_id", updates.optJSONArray(i).optInt(3));
                    intent.putExtra("time", updates.optJSONArray(i).optLong(4));
                    intent.putExtra("body", updates.optJSONArray(i).optString(6));
                    intent.putExtra("attachments", updates.optJSONArray(i).optJSONObject(7).optString("attach1"));
                    intent.putExtra("key", key);
                    sendBroadcast(intent);
                    break;

                case USER_TYPING_DIALOG:
                    intent.setAction(ACTION_USER_TYPING_DIALOG);
                    intent.putExtra("id", updates.optJSONArray(i).optInt(1));
                    intent.putExtra("flag", updates.optJSONArray(i).optInt(2));
                    sendBroadcast(intent);
                    break;

                case MESSAGE_FLAGS_RESET:
                    intent.setAction(ACTION_MESSAGE_FLAGS_RESET);
                    intent.putExtra("message_id", updates.optJSONArray(i).optInt(1));
                    intent.putExtra("flags", updates.optJSONArray(i).optInt(2));
                    intent.putExtra("user_id", updates.optJSONArray(i).optInt(3));
                    sendBroadcast(intent);
                    break;

                case ALL_OUTBOX_MESSAGES_READ:
                    intent.setAction(ACTION_ALL_OUTBOX_MESSAGES_READ);
                    intent.putExtra("peer_id", updates.optJSONArray(i).optInt(1));
                    intent.putExtra("local_id", updates.optJSONArray(i).optInt(2));
                    sendBroadcast(intent);
                    break;

                case ALL_INBOX_MESSAGES_READ:
                    intent.setAction(ACTION_ALL_INBOX_MESSAGES_READ);
                    intent.putExtra("peer_id", updates.optJSONArray(i).optInt(1));
                    intent.putExtra("local_id", updates.optJSONArray(i).optInt(2));
                    sendBroadcast(intent);
                    break;
            }

        }
    }



    public static IntentFilter makeIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(LongPollBroadcastService.ACTION_MESSAGE_ADDED);
        intentFilter.addAction(LongPollBroadcastService.ACTION_USER_TYPING_DIALOG);
        intentFilter.addAction(LongPollBroadcastService.ACTION_USER_TYPING_CHAT);
        intentFilter.addAction(LongPollBroadcastService.ACTION_MESSAGE_FLAGS_RESET);
        intentFilter.addAction(LongPollBroadcastService.ACTION_ALL_OUTBOX_MESSAGES_READ);
        intentFilter.addAction(LongPollBroadcastService.ACTION_ALL_INBOX_MESSAGES_READ);
        //intentFilter.addAction(BluetoothService.ACTION_GATT_DISCONNECTED);
        //intentFilter.addAction(BluetoothService.ACTION_GATT_SERVICES_DISCOVERED);
        //intentFilter.addAction(BluetoothService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

}
